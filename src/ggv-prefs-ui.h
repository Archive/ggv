/*
 * ggv-prefs-ui.h: GGV preferences ui
 *
 * Copyright 2002 - 2005 the Free Software Foundation
 *
 * Author: Jaka Mocnik  <jaka@gnu.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  */

#ifndef __GGV_PREFS_UI_H__
#   define __GGV_PREFS_UI_H__

#   include <config.h>

#   include <glib.h>
#   include <gconf/gconf-client.h>

#   include "ggv-prefs.h"

G_BEGIN_DECLS
#   define GGV_TYPE_PREFS_DIALOG            (ggv_prefs_dialog_get_type ())
#   define GGV_PREFS_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GGV_TYPE_PREFS_DIALOG, GgvPrefsDialog))
#   define GGV_PREFS_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GGV_TYPE_PREFS_DIALOG, GgvPrefsDialogClass))
#   define GGV_IS_PREFS_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GGV_TYPE_PREFS_DIALOG))
#   define GGV_IS_PREFS_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GGV_TYPE_PREFS_DIALOG))
#   define GGV_PREFS_DIALOG_GET_CLASS(o)    (G_TYPE_INSTANCE_GET_CLASS ((o), GGV_TYPE_PREFS_DIALOG, GgvPrefsDialogClass))
typedef struct _GgvPrefsDialog GgvPrefsDialog;
typedef struct _GgvPrefsDialogClass GgvPrefsDialogClass;

struct _GgvPrefsDialog {
  GtkDialog dlg;

  GtkWidget *notebook;

  GtkWidget *gs, *alpha_params, *convert_pdf, *pdf2dsc, *unzip, *unbzip2, *print;   /* entries */
  GtkWidget *size, *zoom, *orientation, *units, *auto_fit;  /* combo boxes */
  GtkWidget *watch, *aa, *override_size, *respect_eof;
  GtkWidget *show_scroll_rect, *page_flip;  /* checkbuttons */
  GtkWidget *override_orientation;
  GtkWidget *sidebar, *mbar, *sbar, *toolbar;
  GtkWidget *savegeo, *auto_jump;   /* checkbuttons */
  GtkWidget *right_panel;
  GtkWidget *scroll_step;
};

struct _GgvPrefsDialogClass {
  GtkDialogClass klass;
};

GType ggv_prefs_dialog_get_type(void);
GtkWidget *ggv_prefs_dialog_new(void);
void ggv_prefs_dialog_show(GgvPrefsDialog * dlg);

G_END_DECLS
#endif /* __GGV_PREFS_UI_H__ */
