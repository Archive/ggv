/*
 * ggv-recent.c
 * This file is part of ggv
 *
 * Copyright (C) 2002 James Willcox
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, 
 * Boston, MA 02111-1307, USA. 
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "ggv-recent.h"

static EggRecentModel *model;

#define GGV_RECENT_MAX_ENTRIES		5

EggRecentModel *
ggv_recent_get_model(void)
{
  return model;
}

void
ggv_recent_init(void)
{
  model = egg_recent_model_new(EGG_RECENT_MODEL_SORT_MRU);
  egg_recent_model_set_limit(model, 5);
  egg_recent_model_set_filter_mime_types(model, "application/postscript", NULL);
}
