/*
 * ggv-prefs.h: GGV preferences
 *
 * Copyright 2002 - 2005 the Free Software Foundation
 *
 * Author: Jaka Mocnik  <jaka@gnu.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  */

#ifndef __GGV_PREFS_H__
#define __GGV_PREFS_H__

#include <config.h>

#include <glib.h>
#include <gconf/gconf-client.h>

G_BEGIN_DECLS

#define DEFAULT_WINDOW_WIDTH  640
#define DEFAULT_WINDOW_HEIGHT 480
#define DEFAULT_FILE_SEL_WIDTH   320
#define DEFAULT_FILE_SEL_HEIGHT  256

extern gchar *ggv_print_cmd;    /* print command: must print its stdin */
extern gboolean ggv_panel;      /* panel visible */
extern gboolean ggv_toolbar;    /* toolbar visible */
extern gboolean ggv_menubar;    /* menubar visible */
extern gboolean ggv_statusbar;  /* statusbar visible */
extern gboolean ggv_save_geometry;  /* Save the current geometry for next session */
extern gint ggv_unit_index;     /* the unit we want to use for coordinates */
extern gboolean ggv_autojump;   /* auto jump to beginning of the page */
extern gboolean ggv_pageflip;   /* automatically flip pages */
extern gboolean ggv_right_panel;    /* panel appears on the left side by default */
extern gboolean ggv_watch_doc;  /* watch doc for changes */

/* default window dimensions */
extern gint ggv_default_width;
extern gint ggv_default_height;

/* this is used _only_ by the ggv-postscript-viewer */
void ggv_prefs_load(void);

/* this is also used by ggv shell */
GConfClient *ggv_prefs_gconf_client(void);

G_END_DECLS

#endif /* __GGV_PREFS_H__ */
