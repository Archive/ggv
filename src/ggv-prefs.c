/*
 * ggv-prefs.c: GGV preferences
 *
 * Copyright 2002 - 2005 The Free Software Foundation
 *
 * Author: Jaka Mocnik  <jaka@gnu.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>

#include "ggv-prefs.h"
#include "ggvutils.h"
#include "gsdefaults.h"

gchar *ggv_print_cmd = NULL;
gboolean ggv_save_geometry = FALSE;
gint ggv_default_width = DEFAULT_WINDOW_WIDTH;
gint ggv_default_height = DEFAULT_WINDOW_HEIGHT;

gboolean ggv_panel = TRUE, ggv_menubar = TRUE, ggv_toolbar = TRUE;
gboolean ggv_statusbar = TRUE;
gboolean ggv_autojump = TRUE, ggv_pageflip = FALSE;
gboolean ggv_right_panel = FALSE, ggv_watch_doc = TRUE;

gint ggv_unit_index = 0;

void
ggv_prefs_load()
{
  GConfClient *gconf_client;

  gtk_gs_defaults_load();

  gconf_client = ggv_prefs_gconf_client();

  if(ggv_print_cmd)
    g_free(ggv_print_cmd);
  ggv_print_cmd =
    gconf_client_get_string(gconf_client, "/apps/ggv/printing/command", NULL);
  if(ggv_print_cmd == NULL || ggv_print_cmd[0] == '\0') {
    ggv_print_cmd = g_strdup(LPR_PATH);

    if(ggv_print_cmd == NULL || ggv_print_cmd[0] == '\0') {
      /* For Solaris, lpr is probably not in PATH */
      const char *lpr_cmd = "/usr/ucb/lpr";
      if(g_file_test(lpr_cmd, G_FILE_TEST_IS_EXECUTABLE)) {
        g_free(ggv_print_cmd);
        ggv_print_cmd = g_strdup(lpr_cmd);
      }
    }
  }

  ggv_unit_index =
    gconf_client_get_int(gconf_client, "/apps/ggv/coordinates/units", NULL);

  /* read ggv widget defaults */
  ggv_panel = gconf_client_get_bool(gconf_client,
                                    "/apps/ggv/layout/showpanel", NULL);
  ggv_toolbar = gconf_client_get_bool(gconf_client,
                                      "/apps/ggv/layout/showtoolbar", NULL);
  ggv_menubar = gconf_client_get_bool(gconf_client,
                                      "/apps/ggv/layout/showmenubar", NULL);
  ggv_statusbar = gconf_client_get_bool(gconf_client,
                                        "/apps/ggv/layout/showstatusbar", NULL);
  ggv_autojump = gconf_client_get_bool(gconf_client,
                                       "/apps/ggv/control/autojump", NULL);
  ggv_pageflip = gconf_client_get_bool(gconf_client,
                                       "/apps/ggv/control/pageflip", NULL);
  ggv_right_panel = gconf_client_get_bool(gconf_client,
                                          "/apps/ggv/layout/rightpanel", NULL);
  ggv_watch_doc = gconf_client_get_bool(gconf_client,
                                        "/apps/ggv/gtkgs/watch_doc", NULL);
  /* Get geometry */
  ggv_save_geometry = gconf_client_get_bool
    (gconf_client, "/apps/ggv/layout/savegeometry", NULL);
  if((ggv_default_width = gconf_client_get_int
      (gconf_client, "/apps/ggv/layout/windowwidth", NULL)) == 0)
    ggv_default_width = DEFAULT_WINDOW_WIDTH;
  if((ggv_default_height = gconf_client_get_int
      (gconf_client, "/apps/ggv/layout/windowheight", NULL)) == 0)
    ggv_default_height = DEFAULT_WINDOW_HEIGHT;
}

static void
ggv_prefs_changed(GConfClient * client, guint cnxn_id,
                  GConfEntry * entry, gpointer user_data)
{
  if(!strcmp(entry->key, "/apps/ggv/printing/command")) {
    if(ggv_print_cmd)
      g_free(ggv_print_cmd);
    ggv_print_cmd = g_strdup(gconf_value_get_string(entry->value));
    if(!ggv_print_cmd)
      ggv_print_cmd = g_strdup(LPR_PATH " %s");
  }
  else if(!strcmp(entry->key, "/apps/ggv/coordinates/units")) {
    ggv_unit_index =
      gconf_client_get_int(client, "/apps/ggv/coordinates/units", NULL);
  }
  else if(!strcmp(entry->key, "/apps/ggv/layout/showpanel")) {
    ggv_panel = gconf_client_get_bool(client,
                                      "/apps/ggv/layout/showpanel", NULL);
  }
  else if(!strcmp(entry->key, "/apps/ggv/layout/showtoolbar")) {
    ggv_toolbar = gconf_client_get_bool(client,
                                        "/apps/ggv/layout/showtoolbar", NULL);
  }
  else if(!strcmp(entry->key, "/apps/ggv/layout/showstatusbar")) {
    ggv_statusbar = gconf_client_get_bool(client,
                                          "/apps/ggv/layout/showstatusbar",
                                          NULL);
  }
  else if(!strcmp(entry->key, "/apps/ggv/layout/showmenubar")) {
    ggv_menubar = gconf_client_get_bool(client,
                                        "/apps/ggv/layout/showmenubar", NULL);
  }
  else if(!strcmp(entry->key, "/apps/ggv/control/autojump")) {
    ggv_autojump = gconf_client_get_bool(client,
                                         "/apps/ggv/control/autojump", NULL);
  }
  else if(!strcmp(entry->key, "/apps/ggv/control/pageflip")) {
    ggv_pageflip = gconf_client_get_bool(client,
                                         "/apps/ggv/control/pageflip", NULL);
  }
  else if(!strcmp(entry->key, "/apps/ggv/layout/rightpanel")) {
    ggv_right_panel = gconf_client_get_bool(client,
                                            "/apps/ggv/layout/rightpanel",
                                            NULL);
  }
  else if(!strcmp(entry->key, "/apps/ggv/layout/savegeometry")) {
    ggv_save_geometry = gconf_client_get_bool
      (client, "/apps/ggv/layout/savegeometry", NULL);
  }
  else if(!strcmp(entry->key, "/apps/ggv/layout/windowwidth")) {
    if((ggv_default_width = gconf_client_get_int
        (client, "/apps/ggv/layout/windowwidth", NULL)) == 0)
      ggv_default_width = DEFAULT_WINDOW_WIDTH;
  }
  else if(!strcmp(entry->key, "/apps/ggv/layout/windowheight")) {
    if((ggv_default_height = gconf_client_get_int
        (client, "/apps/ggv/layout/windowheight", NULL)) == 0)
      ggv_default_height = DEFAULT_WINDOW_HEIGHT;
  }
  else if(!strcmp(entry->key, "/apps/ggv/gtkgs/watch_doc")) {
    ggv_watch_doc = gconf_client_get_bool
      (client, "/apps/ggv/gtkgs/watch_doc", NULL);
  }
}

GConfClient *
ggv_prefs_gconf_client()
{
  static GConfClient *gconf_client = NULL;

  if(!gconf_client) {
    g_assert(gconf_is_initialized());
    gconf_client = gconf_client_get_default();
    g_assert(gconf_client != NULL);
    gconf_client_add_dir(gconf_client, "/apps/ggv",
                         GCONF_CLIENT_PRELOAD_RECURSIVE, NULL);
    gconf_client_notify_add(gconf_client, "/apps/ggv",
                            (GConfClientNotifyFunc) ggv_prefs_changed,
                            NULL, NULL, NULL);
  }

  return gconf_client;
}
