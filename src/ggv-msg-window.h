/*
 * ggv-msg-window.h: display of GhostScript's output
 *
 * Copyright (C) 2001 - 2005 the Free Software Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GGV_MSG_WINDOW_H__

#define __GGV_MSG_WINDOW_H__

#include <gtk/gtk.h>

typedef struct _GgvMsgWindow GgvMsgWindow;

struct _GgvMsgWindow {
  GtkWidget *window;
  GtkWidget *error_text;
};

GgvMsgWindow *ggv_msg_window_new(const gchar * title);
void ggv_msg_window_free(GgvMsgWindow * win);
void ggv_msg_window_init(GgvMsgWindow * win, const gchar * title);
void ggv_msg_window_show(GgvMsgWindow * win);
void ggv_msg_window_add_text(GgvMsgWindow * win, const gchar * text, gint show);

#endif
