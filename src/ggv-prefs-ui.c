/*
 * ggv-prefs-ui.c: GGV preferences ui
 *
 * Copyright 2002 - 2005 The Free Software Foundation
 *
 * Author: Jaka Mocnik  <jaka@gnu.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <config.h>

#include <math.h>

#include "ggv-prefs.h"
#include "ggvutils.h"
#include "gsdefaults.h"
#include "ggv-prefs-ui.h"

static GtkDialogClass *parent_class;

static GConfClient *gconf_client = NULL;

static void
interpreter_activated(GtkWidget * entry, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);

  gtk_gs_defaults_set_interpreter_cmd
    (g_strdup(gtk_entry_get_text(GTK_ENTRY(dlg->gs))));
}

static gboolean
interpreter_focus_out_event(GtkWidget * entry, GdkEventFocus * e,
                            gpointer user_data)
{
  interpreter_activated(entry, user_data);
  return FALSE;
}

static void
pdf2pscmd_activated(GtkWidget * entry, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);

  gtk_gs_defaults_set_convert_pdf_cmd
    (g_strdup(gtk_entry_get_text(GTK_ENTRY(dlg->convert_pdf))));
}

static gboolean
pdf2pscmd_focus_out_event(GtkWidget * entry, GdkEventFocus * e,
                          gpointer user_data)
{
  pdf2pscmd_activated(entry, user_data);
  return FALSE;
}

static void
pdf2dsccmd_activated(GtkWidget * entry, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);

  gtk_gs_defaults_set_dsc_cmd
    (g_strdup(gtk_entry_get_text(GTK_ENTRY(dlg->pdf2dsc))));
}

static gboolean
pdf2dsccmd_focus_out_event(GtkWidget * entry, GdkEventFocus * e,
                           gpointer user_data)
{
  pdf2dsccmd_activated(entry, user_data);
  return FALSE;
}

static void
gzipcmd_activated(GtkWidget * entry, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  gtk_gs_defaults_set_ungzip_cmd
    (g_strdup(gtk_entry_get_text(GTK_ENTRY(dlg->unzip))));
}

static gboolean
gzipcmd_focus_out_event(GtkWidget * entry, GdkEventFocus * e,
                        gpointer user_data)
{
  gzipcmd_activated(entry, user_data);
  return FALSE;
}

static void
bzip2cmd_activated(GtkWidget * entry, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  gtk_gs_defaults_set_unbzip2_cmd
    (g_strdup(gtk_entry_get_text(GTK_ENTRY(dlg->unbzip2))));
}

static gboolean
bzip2cmd_focus_out_event(GtkWidget * entry, GdkEventFocus * e,
                         gpointer user_data)
{
  bzip2cmd_activated(entry, user_data);
  return FALSE;
}

static void
alpha_params_activated(GtkWidget * entry, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  gtk_gs_defaults_set_alpha_parameters
    (g_strdup(gtk_entry_get_text(GTK_ENTRY(dlg->alpha_params))));
}

static gboolean
alpha_params_focus_out_event(GtkWidget * entry, GdkEventFocus * e,
                             gpointer user_data)
{
  alpha_params_activated(entry, user_data);
  return FALSE;
}

static void
scroll_step_changed(GtkWidget * entry, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  gtk_gs_defaults_set_scroll_step
    (gtk_spin_button_get_value(GTK_SPIN_BUTTON(dlg->scroll_step)));
}

static void
printcmd_activated(GtkWidget * entry, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  if(ggv_print_cmd)
    g_free(ggv_print_cmd);
  ggv_print_cmd = g_strdup(gtk_entry_get_text(GTK_ENTRY(dlg->print)));
  gconf_client_set_string(gconf_client, "/apps/ggv/printing/command",
                          ggv_print_cmd, NULL);
}

static gboolean
printcmd_focus_out_event(GtkWidget * entry, GdkEventFocus * e,
                         gpointer user_data)
{
  printcmd_activated(entry, user_data);
  return FALSE;
}

static void
size_combo_box_changed(GtkWidget * combo_box, gpointer user_data)
{
  gtk_gs_defaults_set_size(gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box)));
}

static void
zoom_combo_box_changed(GtkWidget * combo_box, gpointer user_data)
{
  gtk_gs_defaults_set_zoom_factor
    (ggv_zoom_levels[gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box))]);
}


static void
orientation_combo_box_changed(GtkWidget * combo_box, gpointer user_data)
{
  gtk_gs_defaults_set_orientation
    (gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box)));
}


static void
unit_combo_box_changed(GtkWidget * combo_box, gpointer user_data)
{
  ggv_unit_index = gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box));
  gconf_client_set_int(gconf_client, "/apps/ggv/coordinates/units",
                       ggv_unit_index, NULL);
}

static void
autofit_combo_box_changed(GtkWidget * combo_box, gpointer user_data)
{
  gtk_gs_defaults_set_zoom_mode
    (gtk_combo_box_get_active(GTK_COMBO_BOX(combo_box)));
}

static void
respect_eof_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  gtk_gs_defaults_set_respect_eof(GTK_TOGGLE_BUTTON(dlg->respect_eof)->active);
}

static void
watch_doc_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  gconf_client_set_bool(gconf_client, "/apps/ggv/gtkgs/watch_doc",
                        GTK_TOGGLE_BUTTON(dlg->watch)->active, NULL);
}

static void
antialiasing_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  gtk_gs_defaults_set_antialiased(GTK_TOGGLE_BUTTON(dlg->aa)->active);
}

static void
override_size_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  gtk_gs_defaults_set_override_size(GTK_TOGGLE_BUTTON(dlg->override_size)->
                                    active);
}

static void
override_orientation_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  gtk_gs_defaults_set_override_orientation(GTK_TOGGLE_BUTTON
                                           (dlg->override_orientation)->active);
}

static void
show_scroll_rect_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  gtk_gs_defaults_set_show_scroll_rect(GTK_TOGGLE_BUTTON
                                       (dlg->show_scroll_rect)->active);
}



static void
show_side_panel_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  ggv_panel = GTK_TOGGLE_BUTTON(dlg->sidebar)->active;
  gconf_client_set_bool(gconf_client, "/apps/ggv/layout/showpanel",
                        ggv_panel, NULL);
}

static void
save_geometry_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  ggv_save_geometry = GTK_TOGGLE_BUTTON(dlg->savegeo)->active;
  gconf_client_set_bool(gconf_client, "/apps/ggv/layout/savegeometry",
                        ggv_save_geometry, NULL);
  if(ggv_save_geometry) {
    gconf_client_set_int(gconf_client, "/apps/ggv/layout/windowwidth",
                         ggv_default_width, NULL);
    gconf_client_set_int(gconf_client, "/apps/ggv/layout/windowheight",
                         ggv_default_height, NULL);
  }
}

static void
show_toolbar_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  ggv_toolbar = GTK_TOGGLE_BUTTON(dlg->toolbar)->active;
  gconf_client_set_bool(gconf_client, "/apps/ggv/layout/showtoolbar",
                        ggv_toolbar, NULL);
}

static void
show_menubar_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  ggv_menubar = GTK_TOGGLE_BUTTON(dlg->mbar)->active;
  gconf_client_set_bool(gconf_client, "/apps/ggv/layout/showmenubar",
                        ggv_menubar, NULL);
}

static void
show_statusbar_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  ggv_statusbar = GTK_TOGGLE_BUTTON(dlg->sbar)->active;
  gconf_client_set_bool(gconf_client, "/apps/ggv/layout/showstatusbar",
                        ggv_statusbar, NULL);
}

static void
autojump_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  ggv_autojump = GTK_TOGGLE_BUTTON(dlg->auto_jump)->active;
  gconf_client_set_bool(gconf_client, "/apps/ggv/control/autojump",
                        ggv_autojump, NULL);
}

static void
pageflip_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  ggv_pageflip = GTK_TOGGLE_BUTTON(dlg->page_flip)->active;
  gconf_client_set_bool(gconf_client, "/apps/ggv/control/pageflip",
                        ggv_pageflip, NULL);
}

static void
right_panel_clicked(GtkWidget * w, gpointer user_data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(user_data);
  ggv_right_panel = GTK_TOGGLE_BUTTON(dlg->right_panel)->active;
  gconf_client_set_bool(gconf_client, "/apps/ggv/layout/rightpanel",
                        ggv_right_panel, NULL);
}

#if 0
static void
ggv_prefs_dialog_ok_clicked(GtkWidget * widget, gpointer * data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(data);

  gtk_widget_hide(GTK_WIDGET(dlg));
  ggv_prefs_dialog_apply(GGV_PREFS_DIALOG(dlg));
}


static void
ggv_prefs_dialog_apply_clicked(GtkWidget * widget, gpointer * data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(data);

  ggv_prefs_dialog_apply(GGV_PREFS_DIALOG(dlg));
}


static void
ggv_prefs_dialog_cancel_clicked(GtkWidget * widget, gpointer * data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(data);

  gtk_widget_hide(GTK_WIDGET(dlg));
}
#endif /* 0 */

static gboolean
ggv_prefs_dialog_delete_event(GtkWidget * widget, GdkEventAny * event)
{
  gtk_widget_hide(widget);

  return TRUE;
}

static void
ggv_prefs_dialog_class_init(GgvPrefsDialogClass * klass)
{
  parent_class = g_type_class_peek_parent(klass);

  if(gconf_client == NULL)
    gconf_client = ggv_prefs_gconf_client();

  GTK_WIDGET_CLASS(klass)->delete_event = ggv_prefs_dialog_delete_event;
}

static void
ggv_prefs_dialog_init(GgvPrefsDialog * dlg)
{
}

static void
ggv_prefs_dialog_setup(GgvPrefsDialog * dlg)
{
  int i;

  /* We have to find which zoom option to activate. */
  {
    gfloat zoom = gtk_gs_defaults_get_zoom_factor();
    gfloat mindist = 1000.0, dist;
    gint opt = 0;

    for(i = 0; i <= ggv_max_zoom_levels; i++) {
      dist = fabs((double) (ggv_zoom_levels[i] - zoom));
      if(dist < mindist) {
        opt = i;
        mindist = dist;
      }
      gtk_combo_box_set_active(GTK_COMBO_BOX(dlg->zoom), opt);
    }
  }

  /* set unit */
  gtk_combo_box_set_active(GTK_COMBO_BOX(dlg->units), ggv_unit_index);

  /* set auto-fit mode */
  gtk_combo_box_set_active(GTK_COMBO_BOX(dlg->auto_fit),
                           gtk_gs_defaults_get_zoom_mode());

  /* set default size */
  i = gtk_gs_defaults_get_size();
  gtk_combo_box_set_active(GTK_COMBO_BOX(dlg->size), i);

  /* set override size */
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->override_size),
                               gtk_gs_defaults_get_override_size());

  /* set fallback orientation */
  i = gtk_gs_defaults_get_orientation();
  gtk_combo_box_set_active(GTK_COMBO_BOX(dlg->orientation), i);

  /* set override orientation */
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->override_orientation),
                               gtk_gs_defaults_get_override_orientation());

  /* set antialiasing */
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->aa),
                               gtk_gs_defaults_get_antialiased());

  /* set respect EOF */
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->respect_eof),
                               gtk_gs_defaults_get_respect_eof());

  /* set watch document */
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->watch), ggv_watch_doc);

  /* set show scroll rect */
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->show_scroll_rect),
                               gtk_gs_defaults_get_show_scroll_rect());

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->sidebar), ggv_panel);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->mbar), ggv_menubar);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->toolbar), ggv_toolbar);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->sbar), ggv_statusbar);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->savegeo),
                               ggv_save_geometry);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->auto_jump), ggv_autojump);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->page_flip), ggv_pageflip);
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(dlg->right_panel),
                               ggv_right_panel);

  gtk_entry_set_text(GTK_ENTRY(dlg->gs), gtk_gs_defaults_get_interpreter_cmd());
  gtk_entry_set_position(GTK_ENTRY(dlg->gs), 0);
  gtk_entry_set_text(GTK_ENTRY(dlg->alpha_params),
                     gtk_gs_defaults_get_alpha_parameters());
  gtk_entry_set_position(GTK_ENTRY(dlg->alpha_params), 0);
  gtk_entry_set_text(GTK_ENTRY(dlg->convert_pdf),
                     gtk_gs_defaults_get_convert_pdf_cmd());
  gtk_entry_set_position(GTK_ENTRY(dlg->convert_pdf), 0);
  gtk_entry_set_text(GTK_ENTRY(dlg->pdf2dsc), gtk_gs_defaults_get_dsc_cmd());
  gtk_entry_set_position(GTK_ENTRY(dlg->pdf2dsc), 0);
  gtk_entry_set_text(GTK_ENTRY(dlg->unzip), gtk_gs_defaults_get_ungzip_cmd());
  gtk_entry_set_position(GTK_ENTRY(dlg->unzip), 0);
  gtk_entry_set_text(GTK_ENTRY(dlg->unbzip2),
                     gtk_gs_defaults_get_unbzip2_cmd());
  gtk_entry_set_position(GTK_ENTRY(dlg->unbzip2), 0);
  gtk_entry_set_text(GTK_ENTRY(dlg->print), ggv_print_cmd);
  gtk_entry_set_position(GTK_ENTRY(dlg->print), 0);

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(dlg->scroll_step),
                            gtk_gs_defaults_get_scroll_step());
  gtk_window_set_position(GTK_WINDOW(dlg), GTK_WIN_POS_MOUSE);
}

void
ggv_prefs_dialog_show(GgvPrefsDialog * dlg)
{
  if(GTK_WIDGET_VISIBLE(dlg))
    return;

  ggv_prefs_dialog_setup(dlg);
  gtk_widget_show(GTK_WIDGET(dlg));
}

static gboolean
ggv_prefs_escape_pressed(GtkAccelGroup * accel_group,
                         GObject * acceleratable,
                         guint keyval, GdkModifierType modifier, gpointer data)
{
  GgvPrefsDialog *dlg = GGV_PREFS_DIALOG(data);
  gtk_widget_hide(GTK_WIDGET(dlg));
  return TRUE;
}

static void
ggv_prefs_dialog_response(GtkDialog * dlg, gint resp_id, gpointer user_data)
{
  GError *error = NULL;

  if(GTK_RESPONSE_HELP == resp_id) {
    gnome_help_display("ggv.xml", "ggv-settings", &error);
    if(NULL != error) {
      g_warning(error->message);
      g_error_free(error);
    }
  }
  else {
    gtk_widget_hide(GTK_WIDGET(dlg));
  }
}


GtkWidget *
ggv_prefs_dialog_new()
{
  GgvPrefsDialog *dlg;
  GtkWidget *widget;
  GtkWidget *table, *label;
  gint i;
  GtkGSPaperSize *papersizes = gtk_gs_defaults_get_paper_sizes();
  GtkAccelGroup *dlg_accel_group;
  GClosure *closure;
  GtkObject *adj;

  dlg = GGV_PREFS_DIALOG(g_object_new(GGV_TYPE_PREFS_DIALOG, NULL));
  widget = GTK_WIDGET(dlg);
  gtk_container_set_border_width(GTK_CONTAINER(dlg), 2);
  g_signal_connect(G_OBJECT(dlg), "response",
                   G_CALLBACK(ggv_prefs_dialog_response), NULL);

  dlg->notebook = gtk_notebook_new();
  gtk_widget_show(dlg->notebook);
  gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dlg)->vbox), dlg->notebook,
                     2, TRUE, TRUE);

  dlg_accel_group = gtk_accel_group_new();
  closure = g_cclosure_new(G_CALLBACK(ggv_prefs_escape_pressed), dlg, NULL);
  gtk_accel_group_connect(dlg_accel_group, GDK_Escape, 0, 0, closure);
  gtk_window_add_accel_group(GTK_WINDOW(dlg), dlg_accel_group);

  gtk_dialog_add_button(GTK_DIALOG(dlg), GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);

  gtk_dialog_add_button(GTK_DIALOG(dlg), GTK_STOCK_HELP, GTK_RESPONSE_HELP);

  gtk_window_set_title(GTK_WINDOW(dlg), _("GGV Preferences"));

  /* Document page */
  table = gtk_table_new(10, 2, FALSE);

  /* zoom choice menu */
  label = gtk_label_new_with_mnemonic(_("Default _Zoom:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_RIGHT);

  gtk_table_attach(GTK_TABLE(table), label,
                   0, 1, 0, 1,
                   GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);

  gtk_widget_show(label);
  dlg->zoom = gtk_combo_box_new_text();
  gtk_widget_show(dlg->zoom);

  /* We only go as far a as MENU_ZOOM_SIZE-1 because the last option
     is "other" */
  for(i = 0; i <= ggv_max_zoom_levels; i++)
    gtk_combo_box_append_text(GTK_COMBO_BOX(dlg->zoom),
                              ggv_zoom_level_names[i]);
  g_signal_connect(G_OBJECT(dlg->zoom), "changed",
                   G_CALLBACK(zoom_combo_box_changed), dlg);

  gtk_table_attach(GTK_TABLE(table), dlg->zoom,
                   1, 2, 0, 1,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->zoom);

  /* auto-fit choice menu */
  label = gtk_label_new_with_mnemonic(_("A_uto-fit mode:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_RIGHT);

  gtk_table_attach(GTK_TABLE(table), label,
                   0, 1, 1, 2,
                   GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);

  gtk_widget_show(label);
  dlg->auto_fit = gtk_combo_box_new_text();
  gtk_widget_show(dlg->auto_fit);
  for(i = 0; i <= ggv_max_auto_fit_modes; i++)
    gtk_combo_box_append_text(GTK_COMBO_BOX(dlg->auto_fit),
                              _(ggv_auto_fit_modes[i]));
  g_signal_connect(G_OBJECT(dlg->auto_fit), "changed",
                   G_CALLBACK(autofit_combo_box_changed), dlg);

  gtk_table_attach(GTK_TABLE(table), dlg->auto_fit,
                   1, 2, 1, 2,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->auto_fit);

  /* units choice menu */
  label = gtk_label_new_with_mnemonic(_("Coordinate _units:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_RIGHT);

  gtk_table_attach(GTK_TABLE(table), label,
                   0, 1, 2, 3,
                   GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);

  gtk_widget_show(label);
  dlg->units = gtk_combo_box_new_text();
  gtk_widget_show(dlg->units);
  for(i = 0; i <= ggv_max_unit_labels; i++)
    gtk_combo_box_append_text(GTK_COMBO_BOX(dlg->units), _(ggv_unit_labels[i]));
  g_signal_connect(G_OBJECT(dlg->units), "changed",
                   G_CALLBACK(unit_combo_box_changed), dlg);

  gtk_table_attach(GTK_TABLE(table), dlg->units,
                   1, 2, 2, 3,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->units);

  /* size choice menu */
  label = gtk_label_new_with_mnemonic(_("_Fallback page size:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_LEFT);
  gtk_table_attach(GTK_TABLE(table), label,
                   0, 1, 3, 4,
                   GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_widget_show(label);
  dlg->size = gtk_combo_box_new_text();
  gtk_widget_show(dlg->size);
  for(i = 0; papersizes[i].name != NULL; i++)
    gtk_combo_box_append_text(GTK_COMBO_BOX(dlg->size), _(papersizes[i].name));
  g_signal_connect(G_OBJECT(dlg->size), "changed",
                   G_CALLBACK(size_combo_box_changed), dlg);

  gtk_table_attach(GTK_TABLE(table), dlg->size,
                   1, 2, 3, 4,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->size);

  /* override media size */
  dlg->override_size =
    gtk_check_button_new_with_mnemonic(_("Override _document size"));
  gtk_table_attach(GTK_TABLE(table), dlg->override_size, 0, 2, 4, 5,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK | GTK_FILL,
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->override_size), "clicked",
                   G_CALLBACK(override_size_clicked), dlg);
  gtk_widget_show(dlg->override_size);

  /* orientation choice menu */
  label = gtk_label_new_with_mnemonic(_("Fallback _media orientation:"));
  gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_LEFT);
  gtk_table_attach(GTK_TABLE(table), label,
                   0, 1, 5, 6,
                   GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_widget_show(label);

  dlg->orientation = gtk_combo_box_new_text();
  gtk_widget_show(dlg->orientation);
  for(i = 0; i <= ggv_max_orientation_labels; i++)
    gtk_combo_box_append_text(GTK_COMBO_BOX(dlg->orientation),
                              _(ggv_orientation_labels[i]));
  g_signal_connect(G_OBJECT(dlg->orientation), "changed",
                   G_CALLBACK(orientation_combo_box_changed), dlg);

  gtk_table_attach(GTK_TABLE(table), dlg->orientation,
                   1, 2, 5, 6,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->orientation);

  /* override orientation media */
  dlg->override_orientation =
    gtk_check_button_new_with_mnemonic(_("O_verride document orientation"));
  gtk_table_attach(GTK_TABLE(table), dlg->override_orientation, 0, 2, 6, 7,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK | GTK_FILL,
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->override_orientation), "clicked",
                   G_CALLBACK(override_orientation_clicked), dlg);
  gtk_widget_show(dlg->override_orientation);

  /* antialiasing */
  dlg->aa = gtk_check_button_new_with_mnemonic(_("A_ntialiasing"));
  gtk_table_attach(GTK_TABLE(table), dlg->aa,
                   0, 2, 7, 8,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->aa), "clicked",
                   G_CALLBACK(antialiasing_clicked), dlg);
  gtk_widget_show(dlg->aa);

  /* respect EOF */
  dlg->respect_eof =
    gtk_check_button_new_with_mnemonic(_("_Respect EOF comments"));
  gtk_table_attach(GTK_TABLE(table), dlg->respect_eof, 0, 2, 8, 9,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK | GTK_FILL,
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->respect_eof), "clicked",
                   G_CALLBACK(respect_eof_clicked), dlg);
  gtk_widget_show(dlg->respect_eof);

  /* watch file */
  dlg->watch = gtk_check_button_new_with_mnemonic(_("_Watch file"));
  gtk_table_attach(GTK_TABLE(table), dlg->watch,
                   0, 2, 9, 10,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->watch), "clicked",
                   G_CALLBACK(watch_doc_clicked), dlg);
  gtk_widget_show(dlg->watch);

  label = gtk_label_new(_("Document"));
  gtk_widget_show(label);
  gtk_widget_show(table);
  gtk_notebook_append_page(GTK_NOTEBOOK(dlg->notebook), table, label);

  /* Layout page */
  table = gtk_table_new(6, 1, FALSE);

  /* show side panel */
  dlg->sidebar = gtk_check_button_new_with_mnemonic(_("_Show side panel"));
  gtk_table_attach(GTK_TABLE(table), dlg->sidebar,
                   0, 1, 0, 1,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->sidebar), "clicked",
                   G_CALLBACK(show_side_panel_clicked), dlg);
  gtk_widget_show(dlg->sidebar);

  dlg->right_panel =
    gtk_check_button_new_with_mnemonic(_
                                       ("_Put side panel on the right-hand side"));
  gtk_table_attach(GTK_TABLE(table), dlg->right_panel, 0, 1, 1, 2,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK | GTK_FILL,
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->right_panel), "clicked",
                   G_CALLBACK(right_panel_clicked), dlg);
  gtk_widget_show(dlg->right_panel);

  /* show menubar */
  dlg->mbar = gtk_check_button_new_with_mnemonic(_("Show _menubar"));
  gtk_table_attach(GTK_TABLE(table), dlg->mbar,
                   0, 1, 2, 3,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->mbar), "clicked",
                   G_CALLBACK(show_menubar_clicked), dlg);
  gtk_widget_show(dlg->mbar);
  /* show toolbar */
  dlg->toolbar = gtk_check_button_new_with_mnemonic(_("Show _toolbar"));
  gtk_table_attach(GTK_TABLE(table), dlg->toolbar,
                   0, 1, 3, 4,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->toolbar), "clicked",
                   G_CALLBACK(show_toolbar_clicked), dlg);
  gtk_widget_show(dlg->toolbar);

  /* show statusbar */
  dlg->sbar = gtk_check_button_new_with_mnemonic(_("Show statusba_r"));
  gtk_table_attach(GTK_TABLE(table), dlg->sbar,
                   0, 1, 4, 5,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->sbar), "clicked",
                   G_CALLBACK(show_statusbar_clicked), dlg);
  gtk_widget_show(dlg->sbar);

  /* save geometry */
  dlg->savegeo = gtk_check_button_new_with_mnemonic(_("Save _geometry"));
  gtk_table_attach(GTK_TABLE(table), dlg->savegeo,
                   0, 1, 5, 6,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->savegeo), "clicked",
                   G_CALLBACK(save_geometry_clicked), dlg);
  gtk_widget_show(dlg->savegeo);

  label = gtk_label_new(_("Layout"));
  gtk_widget_show(label);
  gtk_widget_show(table);
  gtk_notebook_append_page(GTK_NOTEBOOK(dlg->notebook), table, label);


  /* Navigation page */
  table = gtk_table_new(4, 2, FALSE);

  /* auto jump to beginning of page */
  dlg->auto_jump =
    gtk_check_button_new_with_mnemonic(_("_Jump to beginning of page"));
  gtk_table_attach(GTK_TABLE(table), dlg->auto_jump, 0, 2, 0, 1,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK | GTK_FILL,
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->auto_jump), "clicked",
                   G_CALLBACK(autojump_clicked), dlg);
  gtk_widget_show(dlg->auto_jump);
  /* automatically flip pages */
  dlg->page_flip =
    gtk_check_button_new_with_mnemonic(_("Automatically _flip pages"));
  gtk_table_attach(GTK_TABLE(table), dlg->page_flip, 0, 2, 1, 2,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK | GTK_FILL,
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->page_flip), "clicked",
                   G_CALLBACK(pageflip_clicked), dlg);
  gtk_widget_show(dlg->page_flip);

  /* show previously visible part */
  dlg->show_scroll_rect =
    gtk_check_button_new_with_mnemonic(_
                                       ("Outline _last visible part when scrolling"));
  gtk_table_attach(GTK_TABLE(table), dlg->show_scroll_rect, 0, 2, 9, 10,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL, GTK_SHRINK | GTK_FILL,
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->show_scroll_rect), "clicked",
                   G_CALLBACK(show_scroll_rect_clicked), dlg);
  gtk_widget_show(dlg->show_scroll_rect);

  /* scroll step */
  adj = gtk_adjustment_new(0.0, 0.0, 1.0, 0.01, 0.1, 0.1);
  dlg->scroll_step = gtk_spin_button_new(GTK_ADJUSTMENT(adj), 0.01, 2);
  label = gtk_label_new_with_mnemonic(_("Amount of _visible area to scroll"));
  gtk_table_attach(GTK_TABLE(table), label, 0, 1, 2, 3,
                   GTK_SHRINK | GTK_FILL, GTK_SHRINK | GTK_FILL,
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_table_attach(GTK_TABLE(table), dlg->scroll_step, 1, 2, 2, 3,
                   GTK_SHRINK | GTK_FILL | GTK_EXPAND, GTK_SHRINK | GTK_FILL,
                   GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->scroll_step), "value-changed",
                   G_CALLBACK(scroll_step_changed), dlg);
  gtk_widget_show(dlg->scroll_step);
  gtk_widget_show(label);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->scroll_step);

  label = gtk_label_new(_("Navigation"));
  gtk_widget_show(label);
  gtk_widget_show(table);
  gtk_notebook_append_page(GTK_NOTEBOOK(dlg->notebook), table, label);

  /* GhostScript page */
  table = gtk_table_new(6, 2, FALSE);

  /* interpreter */
  label = gtk_label_new_with_mnemonic(_("_Interpreter:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label,
                   0, 1, 0, 1,
                   GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_widget_show(label);
  dlg->gs = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(table), dlg->gs,
                   1, 2, 0, 1,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->gs), "activate",
                   G_CALLBACK(interpreter_activated), dlg);
  g_signal_connect(G_OBJECT(dlg->gs), "focus-out-event",
                   G_CALLBACK(interpreter_focus_out_event), dlg);
  gtk_widget_show(dlg->gs);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->gs);

  /* antialiasing */
  label = gtk_label_new_with_mnemonic(_("A_ntialiasing:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label,
                   0, 1, 1, 2,
                   GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_widget_show(label);
  dlg->alpha_params = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(table), dlg->alpha_params,
                   1, 2, 1, 2,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->alpha_params), "activate",
                   G_CALLBACK(alpha_params_activated), dlg);
  g_signal_connect(G_OBJECT(dlg->alpha_params), "focus-out-event",
                   G_CALLBACK(alpha_params_focus_out_event), dlg);
  gtk_widget_show(dlg->alpha_params);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->alpha_params);

  /* PDF->DSC command */
  label = gtk_label_new_with_mnemonic(_("Convert _PDF to DSC file:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label,
                   0, 1, 2, 3,
                   GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
#ifdef SHOW_PDF_OPTIONS
  gtk_widget_show(label);
#endif /* SHOW_PDF_OPTIONS */
  dlg->pdf2dsc = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(table), dlg->pdf2dsc,
                   1, 2, 2, 3,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->pdf2dsc), "activate",
                   G_CALLBACK(pdf2dsccmd_activated), dlg);
  g_signal_connect(G_OBJECT(dlg->pdf2dsc), "focus-out-event",
                   G_CALLBACK(pdf2dsccmd_focus_out_event), dlg);
#ifdef SHOW_PDF_OPTIONS
  gtk_widget_show(dlg->pdf2dsc);
#endif /* SHOW_PDF_OPTIONS */
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->pdf2dsc);

  /* scan PDF command */
  label = gtk_label_new_with_mnemonic(_("Convert _PDF to PS:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label,
                   0, 1, 3, 4,
                   GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
#ifdef SHOW_PDF_OPTIONS
  gtk_widget_show(label);
#endif /* SHOW_PDF_OPTIONS */
  dlg->convert_pdf = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(table), dlg->convert_pdf,
                   1, 2, 3, 4,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->convert_pdf), "activate",
                   G_CALLBACK(pdf2pscmd_activated), dlg);
  g_signal_connect(G_OBJECT(dlg->convert_pdf), "focus-out-event",
                   G_CALLBACK(pdf2pscmd_focus_out_event), dlg);
#ifdef SHOW_PDF_OPTIONS
  gtk_widget_show(dlg->convert_pdf);
#endif /* SHOW_PDF_OPTIONS */
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->convert_pdf);

  /* unzip command: gzip */
  label = gtk_label_new_with_mnemonic(_("_Gzip:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label,
                   0, 1, 4, 5,
                   GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_widget_show(label);
  dlg->unzip = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(table), dlg->unzip,
                   1, 2, 4, 5,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->unzip), "activate",
                   G_CALLBACK(gzipcmd_activated), dlg);
  g_signal_connect(G_OBJECT(dlg->unzip), "focus-out-event",
                   G_CALLBACK(gzipcmd_focus_out_event), dlg);
  gtk_widget_show(dlg->unzip);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->unzip);

  /* unzip command: bzip2 */
  label = gtk_label_new_with_mnemonic(_("_Bzip2:"));
  gtk_misc_set_alignment(GTK_MISC(label), 0.0, 0.5);
  gtk_table_attach(GTK_TABLE(table), label,
                   0, 1, 5, 6,
                   GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_widget_show(label);
  dlg->unbzip2 = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(table), dlg->unbzip2,
                   1, 2, 5, 6,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->unbzip2), "activate",
                   G_CALLBACK(bzip2cmd_activated), dlg);
  g_signal_connect(G_OBJECT(dlg->unbzip2), "focus-out-event",
                   G_CALLBACK(bzip2cmd_focus_out_event), dlg);
  gtk_widget_show(dlg->unbzip2);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->unbzip2);

  label = gtk_label_new(_("Ghostscript"));
  gtk_widget_show(label);
  gtk_widget_show(table);
  gtk_notebook_append_page(GTK_NOTEBOOK(dlg->notebook), table, label);

  /* Printing page */
  table = gtk_table_new(1, 2, FALSE);

  /* print command */
  label = gtk_label_new_with_mnemonic(_("_Print command:"));
  gtk_table_attach(GTK_TABLE(table), label,
                   0, 1, 0, 1,
                   GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  gtk_widget_show(label);
  dlg->print = gtk_entry_new();
  gtk_table_attach(GTK_TABLE(table), dlg->print,
                   1, 2, 0, 1,
                   GTK_EXPAND | GTK_SHRINK | GTK_FILL,
                   GTK_SHRINK | GTK_FILL, GNOME_PAD_SMALL, GNOME_PAD_SMALL);
  g_signal_connect(G_OBJECT(dlg->print), "activate",
                   G_CALLBACK(printcmd_activated), dlg);
  g_signal_connect(G_OBJECT(dlg->print), "focus-out-event",
                   G_CALLBACK(printcmd_focus_out_event), dlg);
  gtk_widget_show(dlg->print);
  gtk_label_set_mnemonic_widget(GTK_LABEL(label), dlg->print);

  label = gtk_label_new(_("Printing"));
  gtk_widget_show(label);
  gtk_widget_show(table);

  gtk_notebook_append_page(GTK_NOTEBOOK(dlg->notebook), table, label);

  return widget;
}

GType
ggv_prefs_dialog_get_type(void)
{
  static GType ggv_prefs_dialog_type = 0;

  if(!ggv_prefs_dialog_type) {
    static const GTypeInfo ggv_prefs_dialog_info = {
      sizeof(GgvPrefsDialogClass),
      NULL,                     /* base_init */
      NULL,                     /* base_finalize */
      (GClassInitFunc) ggv_prefs_dialog_class_init,
      NULL,                     /* class_finalize */
      NULL,                     /* class_data */
      sizeof(GgvPrefsDialog),
      0,                        /* n_preallocs */
      (GInstanceInitFunc) ggv_prefs_dialog_init,
    };

    ggv_prefs_dialog_type = g_type_register_static(GTK_TYPE_DIALOG,
                                                   "GgvPrefsDialog",
                                                   &ggv_prefs_dialog_info, 0);
  }

  return ggv_prefs_dialog_type;
}
