/*
 * ggv-window.c: everything describing a single ggv window
 *
 * Copyright 2002 - 2005 the Free Software Foundation
 *
 * Author: Jaka Mocnik  <jaka@gnu.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __GGV_WINDOW_H__
#define __GGV_WINDOW_H__

#include <config.h>

#include <gnome.h>
#include <bonobo-activation/bonobo-activation.h>
#include <libgnomeui/gnome-window-icon.h>
#include <gconf/gconf-client.h>
#include <bonobo.h>
#include <bonobo/bonobo-ui-main.h>
#include <libgnomevfs/gnome-vfs-monitor.h>

#include <math.h>
#include <ctype.h>
#include <time.h>

#include <Ggv.h>

#include "gtkgs.h"

G_BEGIN_DECLS

#define GGV_TYPE_WINDOW            (ggv_window_get_type ())
#define GGV_WINDOW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GGV_TYPE_WINDOW, GgvWindow))
#define GGV_WINDOW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GGV_TYPE_WINDOW, GgvWindowClass))
#define GGV_IS_WINDOW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GGV_TYPE_WINDOW))
#define GGV_IS_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GGV_TYPE_WINDOW))
#define GGV_WINDOW_GET_CLASS(o)    (G_TYPE_INSTANCE_GET_CLASS ((o), GGV_TYPE_WINDOW, GgvWindowClass))

typedef struct _GgvWindow GgvWindow;
typedef struct _GgvWindowClass GgvWindowClass;

struct _GgvWindow {
  BonoboWindow win;

  Bonobo_Control control;
  Bonobo_PropertyBag pb;

  BonoboControlFrame *ctlframe;
  BonoboUIComponent *uic, *popup_uic;

  GtkWidget *hbox, *vbox;
  GtkWidget *statusbar;
  GtkWidget *popup_menu;
  GtkWidget *view;

  gboolean show_menus, show_sidebar, show_toolbar;
  gboolean show_statusbar;

  gboolean fullscreen;
  gint orig_x, orig_y, orig_width, orig_height;
  gboolean orig_ss, orig_sss, orig_sm, orig_st;

  gboolean watch_doc;

  gchar **uris_to_open;
  gchar *filename;
  
  GnomeVFSMonitorHandle *monitor;

  gint current_page, page_count;

};

struct _GgvWindowClass {
  BonoboWindowClass klass;
};

GType ggv_window_get_type(void);
GtkWidget *ggv_window_new(void);
void ggv_window_close(GgvWindow * win);
gboolean ggv_window_load(GgvWindow * win, const gchar * filename);
void ggv_window_sync_toolbar_style(GgvWindow * win);
void ggv_window_set_watch_file(GgvWindow * win, gboolean f);
const GList *ggv_get_window_list(void);
void ggv_window_destroy_all(void);
gboolean ggv_window_reload(GgvWindow * win);

G_END_DECLS

#endif /* __GGV_WINDOW_H__ */
