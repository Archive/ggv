# translation of eu.po to Basque
# translation of ggv.gnome-2-4.po to basque
# Copyright (C) 2001, 2004 Free Software Foundation, Inc.
# Hizkuntza Politikarako Sailburuordetza <hizpol@ej-gv.es>, 2004.
# Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>, 2004, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: eu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-09-09 18:05+0200\n"
"PO-Revision-Date: 2005-09-09 18:05+0200\n"
"Last-Translator: Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>\n"
"Language-Team: Basque <itzulpena@euskalgnu.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.0.2\n"

#: ../bonobo/GNOME_GGV.server.in.in.h:1
msgid "GGV PostScript Document Control"
msgstr "GGV PostScript dokumentuen kontrola"

#: ../bonobo/GNOME_GGV.server.in.in.h:2
msgid "GGV PostScript View"
msgstr "GGV PostScript ikuspegia"

#: ../bonobo/GNOME_GGV.server.in.in.h:3
msgid "GGV PostScript viewer factory"
msgstr "GGV PostScript ikustailearen fabrika"

#: ../bonobo/ggv-control-ui.xml.h:2
#, no-c-format
msgid "15_0%"
msgstr "%15_0"

#: ../bonobo/ggv-control-ui.xml.h:3
msgid "1:1"
msgstr "1:1"

#: ../bonobo/ggv-control-ui.xml.h:5
#, no-c-format
msgid "25%"
msgstr "%25"

#: ../bonobo/ggv-control-ui.xml.h:6
msgid "Change zoom factor to default"
msgstr "Aldatu zoom-faktorea balio lehenetsira"

#: ../bonobo/ggv-control-ui.xml.h:7
msgid "Decrease zoom factor"
msgstr "Txikiagotu zoom-faktorea"

#: ../bonobo/ggv-control-ui.xml.h:8
msgid "Fit"
msgstr "Doitu"

#: ../bonobo/ggv-control-ui.xml.h:9
msgid "Fit Width"
msgstr "Doitu zabalera"

#: ../bonobo/ggv-control-ui.xml.h:10
msgid "Fit _Width"
msgstr "Doitu _zabalera"

#: ../bonobo/ggv-control-ui.xml.h:11
msgid "Fit to Page _Size"
msgstr "Doitu _orrialde-tamainari"

#: ../bonobo/ggv-control-ui.xml.h:12
msgid "Fit to Page _Width"
msgstr "Doitu o_rrialde-zabalerari"

#: ../bonobo/ggv-control-ui.xml.h:13
msgid "In"
msgstr "Handiagotu"

#: ../bonobo/ggv-control-ui.xml.h:14
msgid "Increase zoom factor"
msgstr "Handiagotu zoom-faktorea"

#: ../bonobo/ggv-control-ui.xml.h:15
msgid "Other"
msgstr "Bestelakoa"

#: ../bonobo/ggv-control-ui.xml.h:16
msgid "Out"
msgstr "Txikiagotu"

#: ../bonobo/ggv-control-ui.xml.h:17
msgid "Zoom _In"
msgstr "Zooma _handiagotu"

#: ../bonobo/ggv-control-ui.xml.h:18
msgid "Zoom _Out"
msgstr "Zooma _txikiagotu"

#: ../bonobo/ggv-control-ui.xml.h:19
msgid "Zoom the page to be as wide as the window"
msgstr "Leihoaren zabalera osoa hartzeko doitzen du orrialdea"

#: ../bonobo/ggv-control-ui.xml.h:20
msgid "Zoom the page to fit in the window"
msgstr "Leiho osoa hartzeko doitzen du orrialdea"

#: ../bonobo/ggv-control-ui.xml.h:22
#, no-c-format
msgid "_100%"
msgstr "%_100"

#: ../bonobo/ggv-control-ui.xml.h:24
#, no-c-format
msgid "_200%"
msgstr "%_200"

#: ../bonobo/ggv-control-ui.xml.h:26
#, no-c-format
msgid "_400%"
msgstr "%_400"

#: ../bonobo/ggv-control-ui.xml.h:28
#, no-c-format
msgid "_50%"
msgstr "%_50"

#: ../bonobo/ggv-control-ui.xml.h:30
#, no-c-format
msgid "_75%"
msgstr "%_75"

#: ../bonobo/ggv-control-ui.xml.h:31
msgid "_Auto-fit"
msgstr "_Doitu automatikoki"

#: ../bonobo/ggv-control-ui.xml.h:32
msgid "_Default Zoom"
msgstr "_Zoom lehenetsia"

#: ../bonobo/ggv-control-ui.xml.h:33
msgid "_Fit Window"
msgstr "D_oitu leihoari"

#: ../bonobo/ggv-control-ui.xml.h:34
msgid "_No Auto-fit"
msgstr "_Ez doitu automatikoki"

#: ../bonobo/ggv-control-ui.xml.h:35
msgid "_Zoom"
msgstr "_Zooma"

#: ../bonobo/ggv-postscript-view-ui.xml.h:1 ../src/ggvutils.c:59
msgid "10x14"
msgstr "10x14"

#: ../bonobo/ggv-postscript-view-ui.xml.h:2 ../src/ggvutils.c:49
msgid "A0"
msgstr "A0"

#: ../bonobo/ggv-postscript-view-ui.xml.h:3 ../src/ggvutils.c:50
msgid "A1"
msgstr "A1"

#: ../bonobo/ggv-postscript-view-ui.xml.h:4 ../src/ggvutils.c:51
msgid "A2"
msgstr "A2"

#: ../bonobo/ggv-postscript-view-ui.xml.h:5 ../src/ggvutils.c:52
msgid "A3"
msgstr "A3"

#: ../bonobo/ggv-postscript-view-ui.xml.h:6 ../src/ggvutils.c:53
msgid "A4"
msgstr "A4"

#: ../bonobo/ggv-postscript-view-ui.xml.h:7 ../src/ggvutils.c:54
msgid "A5"
msgstr "A5"

#: ../bonobo/ggv-postscript-view-ui.xml.h:8 ../src/ggvutils.c:55
msgid "B4"
msgstr "B4"

#: ../bonobo/ggv-postscript-view-ui.xml.h:9 ../src/ggvutils.c:56
msgid "B5"
msgstr "B5"

#: ../bonobo/ggv-postscript-view-ui.xml.h:10 ../src/ggvutils.c:42
msgid "BBox"
msgstr "BBox"

#: ../bonobo/ggv-postscript-view-ui.xml.h:11 ../src/ggvutils.c:48
msgid "Executive"
msgstr "Exekutiboa"

#: ../bonobo/ggv-postscript-view-ui.xml.h:12
msgid "First"
msgstr "Lehena"

#: ../bonobo/ggv-postscript-view-ui.xml.h:13 ../src/ggvutils.c:57
msgid "Folio"
msgstr "Folioa"

#: ../bonobo/ggv-postscript-view-ui.xml.h:14
msgid "Go to first page"
msgstr "Joan lehen orrialdera"

#: ../bonobo/ggv-postscript-view-ui.xml.h:15
msgid "Go to last page"
msgstr "Joan azken orrialdera"

#: ../bonobo/ggv-postscript-view-ui.xml.h:16
msgid "Go to next page"
msgstr "Joan hurrengo orrialdera"

#: ../bonobo/ggv-postscript-view-ui.xml.h:17
msgid "Go to previous page"
msgstr "Joan aurreko orrialdera"

#: ../bonobo/ggv-postscript-view-ui.xml.h:18
msgid "Last"
msgstr "Azkena"

#: ../bonobo/ggv-postscript-view-ui.xml.h:19 ../src/ggvutils.c:45
msgid "Ledger"
msgstr "Orri bikoitza"

#: ../bonobo/ggv-postscript-view-ui.xml.h:20 ../src/ggvutils.c:46
msgid "Legal"
msgstr "Legala"

#: ../bonobo/ggv-postscript-view-ui.xml.h:21 ../src/ggvutils.c:43
msgid "Letter"
msgstr "Eskutitza"

#: ../bonobo/ggv-postscript-view-ui.xml.h:22
msgid "Next"
msgstr "Hurrengoa"

#: ../bonobo/ggv-postscript-view-ui.xml.h:23
msgid "Open the preferences dialog"
msgstr "Ireki hobespenen elkarrizketa-koadroa"

#: ../bonobo/ggv-postscript-view-ui.xml.h:24
msgid "Post_Script Viewer Preferences"
msgstr "Post_Script ikustailearen hobespenak"

#: ../bonobo/ggv-postscript-view-ui.xml.h:25
msgid "Prev"
msgstr "Aurrekoa"

#: ../bonobo/ggv-postscript-view-ui.xml.h:26
msgid "Print"
msgstr "Inprimatu"

#: ../bonobo/ggv-postscript-view-ui.xml.h:27
msgid "Print marked pages"
msgstr "Markatutako orrialdeak inprimatzen ditu"

#: ../bonobo/ggv-postscript-view-ui.xml.h:28
msgid "Print the whole document"
msgstr "Dokumentu osoa inprimatzen du"

#: ../bonobo/ggv-postscript-view-ui.xml.h:29 ../src/ggvutils.c:58
msgid "Quarto"
msgstr "Laurdena"

#: ../bonobo/ggv-postscript-view-ui.xml.h:30
msgid "Save marked pages to a file"
msgstr "Markatutako orrialdeak fitxategi batean gordetzen ditu"

#: ../bonobo/ggv-postscript-view-ui.xml.h:31 ../src/ggvutils.c:47
msgid "Statement"
msgstr "Faktura"

#: ../bonobo/ggv-postscript-view-ui.xml.h:32 ../src/ggvutils.c:44
msgid "Tabloid"
msgstr "Tabloidea"

#: ../bonobo/ggv-postscript-view-ui.xml.h:33
msgid "_Document Orientation"
msgstr "_Dokumentuaren orientazioa"

#: ../bonobo/ggv-postscript-view-ui.xml.h:34
msgid "_Document Size"
msgstr "Do_kumentuaren tamaina"

#: ../bonobo/ggv-postscript-view-ui.xml.h:35 ../src/ggv-ui.xml.h:23
msgid "_Edit"
msgstr "_Editatu"

#: ../bonobo/ggv-postscript-view-ui.xml.h:36 ../src/ggv-ui.xml.h:24
msgid "_File"
msgstr "_Fitxategia"

#: ../bonobo/ggv-postscript-view-ui.xml.h:37
msgid "_First page"
msgstr "_Lehen orrialdea"

#: ../bonobo/ggv-postscript-view-ui.xml.h:38
msgid "_Go"
msgstr "_Joan"

#: ../bonobo/ggv-postscript-view-ui.xml.h:39
msgid "_Landscape"
msgstr "_Horizontala"

#: ../bonobo/ggv-postscript-view-ui.xml.h:40
msgid "_Last page"
msgstr "_Azken orrialdea"

#: ../bonobo/ggv-postscript-view-ui.xml.h:41
msgid "_Next page"
msgstr "H_urrengo orrialdea"

#: ../bonobo/ggv-postscript-view-ui.xml.h:42
msgid "_Orientation"
msgstr "_Orientazioa"

#: ../bonobo/ggv-postscript-view-ui.xml.h:43
msgid "_Portrait"
msgstr "_Bertikala"

#: ../bonobo/ggv-postscript-view-ui.xml.h:44
msgid "_Previous page"
msgstr "_Aurreko orrialdea"

#: ../bonobo/ggv-postscript-view-ui.xml.h:45
msgid "_Print document"
msgstr "_Inprimatu dokumentua"

#: ../bonobo/ggv-postscript-view-ui.xml.h:46
msgid "_Print marked pages"
msgstr "Inprima_tu markatutako orrialdeak"

#: ../bonobo/ggv-postscript-view-ui.xml.h:47
msgid "_Save marked pages"
msgstr "_Gorde markatutako orrialdeak"

#: ../bonobo/ggv-postscript-view-ui.xml.h:48
msgid "_Seascape"
msgstr "E_tzanda"

#: ../bonobo/ggv-postscript-view-ui.xml.h:49
msgid "_Size"
msgstr "Ta_maina"

#: ../bonobo/ggv-postscript-view-ui.xml.h:50
msgid "_Upside down"
msgstr "B_uruz behera"

#: ../bonobo/ggv-postscript-view-ui.xml.h:51 ../src/ggv-ui.xml.h:30
msgid "_View"
msgstr "_Ikusi"

#: ../bonobo/ggv-postscript-view.c:874
#, c-format
msgid ""
"Unable to execute print command:\n"
"%s"
msgstr ""
"Ezin da inprimatzeko komandoa exekutatu:\n"
"%s"

#: ../bonobo/ggv-postscript-view.c:931
msgid ""
"No pages have been marked.\n"
"Do you want to save the whole document?"
msgstr ""
"Ez da orrialderik markatu.\n"
"Dokumentu osoa gorde nahi duzu?"

#: ../bonobo/ggv-postscript-view.c:956
msgid "Select a file to save pages as"
msgstr "Hautatu fitxategi bat orrialdeak gordetzeko"

#: ../bonobo/ggv-postscript-view.c:964 ../src/ggv-window.c:330
msgid "PostScript Documents"
msgstr "PostScript dokumentuak"

#: ../bonobo/ggv-postscript-view.c:970 ../src/ggv-window.c:336
msgid "All Files"
msgstr "Fitxategi guztiak"

#: ../bonobo/ggv-postscript-view.c:1009
msgid ""
"A file with this name already exists.\n"
"Do you want to overwrite it?"
msgstr ""
"Lehendik badago izen hori duen fitxategi bat.\n"
"Gainidatzi nahi duzu?"

#: ../bonobo/ggv-postscript-view.c:1050
msgid ""
"No pages have been marked.\n"
"Do you want to print the whole document?"
msgstr ""
"Ez da orrialderik markatu.\n"
"Dokumentu osoa inprimatu nahi duzu?"

#: ../bonobo/ggv-postscript-view.c:2117
msgid "GhostScript output"
msgstr "GhostScript irteera"

#: ../bonobo/ggv-postscript-view.c:2149
msgid "Document title"
msgstr "Dokumentuaren titulua"

#: ../bonobo/ggv-postscript-view.c:2152
msgid "GGV control status"
msgstr "GGV kontrolaren egoera"

#: ../bonobo/ggv-postscript-view.c:2155
msgid "Number of pages"
msgstr "Orrialde-kopurua"

#: ../bonobo/ggv-postscript-view.c:2158
msgid "Page names"
msgstr "Orrialde-izenak"

#: ../bonobo/ggv-postscript-view.c:2161
msgid "Current page number"
msgstr "Uneko orrialde-zenbakia"

#: ../bonobo/ggv-postscript-view.c:2164
msgid "Document width"
msgstr "Dokumentuaren zabalera"

#: ../bonobo/ggv-postscript-view.c:2167
msgid "Document height"
msgstr "Dokumentuaren altuera"

#: ../bonobo/ggv-postscript-view.c:2171
msgid "Document orientation"
msgstr "Dokumentuaren orientazioa"

#: ../bonobo/ggv-postscript-view.c:2174
msgid "Default orientation"
msgstr "Orientazio lehenetsia"

#: ../bonobo/ggv-postscript-view.c:2178
msgid "Default size"
msgstr "Tamaina lehenetsia"

#: ../bonobo/ggv-postscript-view.c:2183
msgid "Override document orientation"
msgstr "Jaramonik ez dokumentuaren orientazioari"

#: ../bonobo/ggv-postscript-view.c:2187
msgid "Override document size"
msgstr "Jaramonik ez dokumentuaren tamainari"

#: ../bonobo/ggv-postscript-view.c:2191 ../ggv.schemas.in.h:21
msgid "Respect EOF comment"
msgstr "Errespetatu fitxategi-bukaerako EOF iruzkina"

#: ../bonobo/ggv-postscript-view.c:2195
msgid "Antialiasing"
msgstr "Antialiasing-a"

#: ../bonobo/ggv-sidebar.c:358
msgid "Toggle marked state of all pages"
msgstr "Aldatu orrialde guztien markatu-egoera"

#: ../bonobo/ggv-sidebar.c:359
msgid ""
"Toggle marked state of all pages: previously marked pages will be unmarked "
"and unmarked ones will become marked."
msgstr ""
"Aldatu orrialde guztien markatu-egoera: markatuta dauden orrialdeei marka "
"kenduko zaie, eta markatu gabeak markatu egingo dira."

#: ../bonobo/ggv-sidebar.c:371
msgid "Toggle marked state of odd pages"
msgstr "Aldatu orrialde bakoitien markatu-egoera"

#: ../bonobo/ggv-sidebar.c:372
msgid ""
"Toggle marked state of odd pages: previously marked odd pages will be "
"unmarked and unmarked ones will become marked."
msgstr ""
"Aldatu orrialde bakoitien markatu-egoera: markatuta dauden orrialde "
"bakoitiei marka kenduko zaie, eta markatu gabeak markatu egingo dira."

#: ../bonobo/ggv-sidebar.c:386
msgid "Toggle marked state of even pages"
msgstr "Aldatu orrialde bikoitien markatu-egoera"

#: ../bonobo/ggv-sidebar.c:387
msgid ""
"Toggle marked state of even pages: previously marked even pages will be "
"unmarked and unmarked ones will become marked."
msgstr ""
"Aldatu orrialde bikoitien markatu-egoera: markatuta dauden orrialde "
"bikoitiei marka kenduko zaie, eta markatu gabeak markatu egingo dira."

#: ../bonobo/ggv-sidebar.c:401
msgid "Clear marked state of all pages"
msgstr "Garbitu orrialde guztien markatu-egoera"

#: ../bonobo/ggv-sidebar.c:402
msgid "Clear marked state of all pages: all pages will be unmarked."
msgstr ""
"Garbitu orrialde guztien markatu-egoera: orrialde guztiei marka kenduko zaie."

#: ../ggv.desktop.in.h:1
msgid "PostScript Viewer"
msgstr "PostScript ikustailea"

#: ../ggv.desktop.in.h:2
msgid "View PostScript files"
msgstr "Ikusi PostScript fitxategiak"

#: ../ggv.schemas.in.h:1
msgid "Antialiased rendering"
msgstr "Antialiasing bidezko errendatzea"

#: ../ggv.schemas.in.h:2
msgid "Antialiasing arguments"
msgstr "Antialiasing-aren argumentuak"

#: ../ggv.schemas.in.h:3
msgid "Autojump"
msgstr "Jauzi automatikoa"

#: ../ggv.schemas.in.h:4
msgid "Command line arguments for antialiasing."
msgstr "Antialiasing-aren komando-lerroko argumentuak"

#: ../ggv.schemas.in.h:5
msgid "Command line used to execute the PostScript interpreter."
msgstr "PostScript interpretatzailea exekutatzeko komando-lerroa."

#: ../ggv.schemas.in.h:6
msgid "Coordinate units"
msgstr "Koordenatuen unitateak"

#: ../ggv.schemas.in.h:7
msgid "Default document orientation"
msgstr "Dokumentuaren orientazio lehenetsia"

#: ../ggv.schemas.in.h:8
msgid "Default page size"
msgstr "Orrialdearen tamaina lehenetsia"

#: ../ggv.schemas.in.h:9
msgid "Default zoom factor"
msgstr "Zoom-faktore lehenetsia"

#: ../ggv.schemas.in.h:10
msgid ""
"If true, the orientation specified by the document will be overriden with "
"the default orientation."
msgstr ""
"Egiazkoa bada, ez zaio jaramonik egingo dokumentuan zehaztutako "
"orientazioari, eta lehenetsia erabiliko da."

#: ../ggv.schemas.in.h:11
msgid "Override document specified orientation"
msgstr "Jaramonik ez dokumentuan zehaztutako orientazioari"

#: ../ggv.schemas.in.h:12
msgid "Override document specified page size"
msgstr "Jaramonik ez dokumentuan zehaztutako orrialde-tamainari"

#: ../ggv.schemas.in.h:13
msgid "PDF to DSC conversion command"
msgstr "PDFtik DSCra bihurtzeko komandoa"

#: ../ggv.schemas.in.h:14
msgid "PDF to PS conversion command"
msgstr "PDFtik PSra bihurtzeko komandoa"

#: ../ggv.schemas.in.h:15
msgid "Page flip"
msgstr "Pasatu orrialdea"

#: ../ggv.schemas.in.h:16
msgid "Panel on right-hand side"
msgstr "Eskuineko panela"

#: ../ggv.schemas.in.h:17
msgid "Percentage of the viewable area to move when scrolling."
msgstr "Korritzean mugitzeko area ikusgarriaren ehunekoa."

#: ../ggv.schemas.in.h:18
msgid "Percentage of the viewable area to scroll"
msgstr "Korritzeko area ikusgarriaren ehunekoa"

#: ../ggv.schemas.in.h:19
msgid "PostScript interpreter command"
msgstr "PostScript interpretatzailearen komandoa"

#: ../ggv.schemas.in.h:20
msgid "Printing command"
msgstr "Inprimatzeko komandoa"

#: ../ggv.schemas.in.h:22
msgid "Save window geometry"
msgstr "Gorde leihoaren geometria"

#: ../ggv.schemas.in.h:23
msgid "Show menubar"
msgstr "Erakutsi menu-barra"

#: ../ggv.schemas.in.h:24
msgid "Show previous visible area when scrolling"
msgstr "Korritzean erakutsi aurreko area ikusgarria"

#: ../ggv.schemas.in.h:25
msgid "Show side panel"
msgstr "Erakutsi alboko panela"

#: ../ggv.schemas.in.h:26
msgid "Show statusbar"
msgstr "Erakutsi egoera-barra"

#: ../ggv.schemas.in.h:27
msgid "Show toolbar"
msgstr "Erakutsi tresna-barra"

#: ../ggv.schemas.in.h:28
msgid "The command line used to convert PDF files to PostScript."
msgstr "PDF fitxategiak PostScript bihurtzeko komando-lerroa."

#: ../ggv.schemas.in.h:29
msgid "The command line used to create PostScript DSC files from PDF files."
msgstr ""
"PDF fitxategietatik DSC PostScript fitxategiak sortzeko komando-lerroa."

#: ../ggv.schemas.in.h:30
msgid "The command used to unpack files compressed with bzip2."
msgstr "bzip2-rekin konprimatutako fitxategiak deskonprimatzeko komandoa."

#: ../ggv.schemas.in.h:31
msgid "The command used to unpack files compressed with gzip."
msgstr "gzip-ekin konprimatutako fitxategiak deskonprimatzeko komandoa."

#: ../ggv.schemas.in.h:32
msgid ""
"The default page size to use when no page size is specified by the document "
"or when the user wishes to override the size specified in the document."
msgstr ""
"Dokumentuan tamainarik zehazten ez denean edo dokumentuan zehaztutakoari "
"jaramonik egin nahi ez zaionean erabiliko den orrialde-tamaina lehenetsia."

#: ../ggv.schemas.in.h:33
msgid ""
"The default zoom factor for the document. The value should belong to the "
"interval [0.05, 30.0]."
msgstr ""
"Dokumentuaren zoom-faktore lehenetsia. Balioak [0,05 - 30,0] bitartekoa izan "
"behar du."

#: ../ggv.schemas.in.h:34
msgid "The height of the viewer's window."
msgstr "Ikustailearen leihoaren altuera."

#: ../ggv.schemas.in.h:35
msgid ""
"The orientation to use when the document defines no orientation or when the "
"user wishes to override the document specified orientation."
msgstr ""
"Dokumentuan orientaziorik zehazten ez denean edo dokumentuan zehaztutakoari "
"jaramonik egin nahi ez zaionean erabiliko den orientazioa."

#: ../ggv.schemas.in.h:36
msgid "The shell command used to print files."
msgstr "Fitxategiak inprimatzeko shell komandoa."

#: ../ggv.schemas.in.h:37
msgid "The width of the viewer's window."
msgstr "Ikustailearen leihoaren zabalera."

#: ../ggv.schemas.in.h:38
msgid "Toolbar style"
msgstr "Tresna-barraren estiloa"

#: ../ggv.schemas.in.h:39
msgid "Unbzip2 command"
msgstr "Unbzip2 komandoa"

#: ../ggv.schemas.in.h:40
msgid "Ungzip command"
msgstr "Ungzip komandoa"

#: ../ggv.schemas.in.h:41
msgid "Units for the coordinate display."
msgstr "Koordenatuak bistaratzeko unitateak."

#: ../ggv.schemas.in.h:42
msgid "Viewer window height"
msgstr "Ikustailearen leihoaren altuera"

#: ../ggv.schemas.in.h:43
msgid "Viewer window width"
msgstr "Ikustailearen leihoaren zabalera"

#: ../ggv.schemas.in.h:44
msgid "Watch document for changes"
msgstr "Begiratu dokumentuan egindako aldaketei"

#: ../ggv.schemas.in.h:45
msgid ""
"Whether GGV should automatically flip pages when you scroll to the edge."
msgstr ""
"Ertzera iritsitakoan GGVk automatikoki hurrengo/aurreko orrialdera joan "
"behar duen ala ez."

#: ../ggv.schemas.in.h:46
msgid ""
"Whether the PostScript display widget should poll the viewed file for "
"changes and reload it if it changes."
msgstr ""
"PostScript-en bistaratzaileak fitxategian aldaketarik egin den begiratu "
"behar duen, eta, egin bada, berriro kargatu behar duen ala ez."

#: ../ggv.schemas.in.h:47
msgid ""
"Whether the PostScript display widget should use its default page size "
"instead of the one specified in the document."
msgstr ""
"PostScript-en bistaratzaileak dokumentuan zehaztutakoaren ordez orrialde-"
"tamaina lehenetsia erabili behar duen ala ez."

#: ../ggv.schemas.in.h:48
msgid ""
"Whether the PostScript display widget will respect the End Of File comment. "
"If true, the widget will only parse the document until EOF comment."
msgstr ""
"PostScript-en bistaratzaileak fitxategi-amaierako EOF iruzkina errespetatu "
"behar duen ala ez. Hautatuta badago, EOF aurkitu arte bakarrik analizatuko "
"du dokumentua."

#: ../ggv.schemas.in.h:49
msgid ""
"Whether the control should jump to next page when using keyboard to scroll."
msgstr ""
"Teklatuaren bidez korritzen denean kontrolak hurrengo orrialdera jauzi egin "
"behar duen ala ez."

#: ../ggv.schemas.in.h:50
msgid "Whether the document should be rendered in an antialiased mode."
msgstr "Dokumentua errendatzeko antialiasing-a egin behar den ala ez."

#: ../ggv.schemas.in.h:51
msgid "Whether the menubar should be visible by default."
msgstr "Menu-barra lehenespenez ikusgai egongo den ala ez."

#: ../ggv.schemas.in.h:52
msgid "Whether the side panel should appear on the right of the display area."
msgstr "Alboko panelak pantailaren eskuinaldean egon behar duen ala ez."

#: ../ggv.schemas.in.h:53
msgid "Whether the side panel should be visible by default."
msgstr "Alboko panela lehenespenez ikusgai egongo den ala ez."

#: ../ggv.schemas.in.h:54
msgid "Whether the statusbar should be visible by default."
msgstr "Egoera-barra lehenespenez ikusgai egongo den ala ez."

#: ../ggv.schemas.in.h:55
msgid ""
"Whether the toolbar buttons show icons, text, both or use the GNOME-wide "
"default."
msgstr ""
"Tresna-barrako botoietan ikonoak, testua edo biak ikusiko diren edo GNOMEren "
"ezarpen lehenetsia erabiliko den."

#: ../ggv.schemas.in.h:56
msgid "Whether the toolbar should be visible by default."
msgstr "Tresna-barra lehenespenez ikusgai egongo den ala ez."

#: ../ggv.schemas.in.h:57
msgid ""
"Whether the user wants to see a rectangle outlining the previously visible "
"area when scrolling."
msgstr ""
"Korritzean, lehen ikusgai zegoen area inguratzen duen laukizuzen bat "
"bistaratu behar den ala ez."

#: ../ggv.schemas.in.h:58
msgid "Whether the viewer should save and restore the window geometry."
msgstr ""
"Ikustaileak leihoaren geometria gorde eta leheneratu behar duen ala ez."

#: ../src/ggv-prefs-ui.c:541
msgid "GGV Preferences"
msgstr "GGVren hobespenak"

#. zoom choice menu
#: ../src/ggv-prefs-ui.c:547
msgid "Default _Zoom:"
msgstr "_Zoom lehenetsia:"

#. auto-fit choice menu
#: ../src/ggv-prefs-ui.c:575
msgid "A_uto-fit mode:"
msgstr "_Doitze automatikoaren modua:"

#. units choice menu
#: ../src/ggv-prefs-ui.c:600
msgid "Coordinate _units:"
msgstr "_Koordenatuen unitateak:"

#. size choice menu
#: ../src/ggv-prefs-ui.c:624
msgid "_Fallback page size:"
msgstr "_Orrialde-tamaina lehenetsia:"

#: ../src/ggv-prefs-ui.c:647
msgid "Override _document size"
msgstr "Jaramonik ez _dokumentuaren tamainari"

#. orientation choice menu
#: ../src/ggv-prefs-ui.c:656
msgid "Fallback _media orientation:"
msgstr "_Paperaren orientazio lehenetsia:"

#: ../src/ggv-prefs-ui.c:680
msgid "O_verride document orientation"
msgstr "_Jaramonik ez dokumentuaren orientazioari"

#. antialiasing
#: ../src/ggv-prefs-ui.c:689
msgid "A_ntialiasing"
msgstr "A_ntialiasing-a"

#: ../src/ggv-prefs-ui.c:700
msgid "_Respect EOF comments"
msgstr "_Errespetatu fitxategi bukaerako EOF iruzkinak"

#. watch file
#: ../src/ggv-prefs-ui.c:709
msgid "_Watch file"
msgstr "Be_hatu fitxategia"

#: ../src/ggv-prefs-ui.c:718
msgid "Document"
msgstr "Dokumentua"

#. show side panel
#: ../src/ggv-prefs-ui.c:727
msgid "_Show side panel"
msgstr "_Erakutsi alboko panela"

#: ../src/ggv-prefs-ui.c:738
msgid "_Put side panel on the right-hand side"
msgstr "E_zarri alboko panela eskuinaldean"

#. show menubar
#: ../src/ggv-prefs-ui.c:747
msgid "Show _menubar"
msgstr "Era_kutsi menu-barra"

#. show toolbar
#: ../src/ggv-prefs-ui.c:756
msgid "Show _toolbar"
msgstr "Erakutsi _tresna-barra"

#. show statusbar
#: ../src/ggv-prefs-ui.c:766
msgid "Show statusba_r"
msgstr "Erakut_si egoera-barra"

#. save geometry
#: ../src/ggv-prefs-ui.c:776
msgid "Save _geometry"
msgstr "_Gorde geometria"

#: ../src/ggv-prefs-ui.c:785
msgid "Layout"
msgstr "Diseinua"

#: ../src/ggv-prefs-ui.c:796
msgid "_Jump to beginning of page"
msgstr "_Joan orrialdearen hasierara"

#: ../src/ggv-prefs-ui.c:805
msgid "Automatically _flip pages"
msgstr "Automatikoki _pasatu orrialdeak"

#: ../src/ggv-prefs-ui.c:816
msgid "Outline _last visible part when scrolling"
msgstr "Korritzean _inguratu ageriko azken zatia"

#: ../src/ggv-prefs-ui.c:827
msgid "Amount of _visible area to scroll"
msgstr "Korritzean agertuko den area-_zatia"

#: ../src/ggv-prefs-ui.c:840
msgid "Navigation"
msgstr "Nabigazioa"

#. interpreter
#: ../src/ggv-prefs-ui.c:849
msgid "_Interpreter:"
msgstr "_Interpretatzailea:"

#. antialiasing
#: ../src/ggv-prefs-ui.c:869
msgid "A_ntialiasing:"
msgstr "A_ntialiasing-a:"

#. PDF->DSC command
#: ../src/ggv-prefs-ui.c:889
msgid "Convert _PDF to DSC file:"
msgstr "Bihurtu _PDFak DSC fitxategi:"

#. scan PDF command
#: ../src/ggv-prefs-ui.c:913
msgid "Convert _PDF to PS:"
msgstr "Bihurtu _PDFak PS:"

#. unzip command: gzip
#: ../src/ggv-prefs-ui.c:937
msgid "_Gzip:"
msgstr "_Gzip:"

#. unzip command: bzip2
#: ../src/ggv-prefs-ui.c:957
msgid "_Bzip2:"
msgstr "_Bzip2:"

#: ../src/ggv-prefs-ui.c:976
msgid "Ghostscript"
msgstr "Ghostscript"

#. print command
#: ../src/ggv-prefs-ui.c:985
msgid "_Print command:"
msgstr "_Inprimatzeko komandoa:"

#: ../src/ggv-prefs-ui.c:1003
msgid "Printing"
msgstr "Inprimatzea"

#: ../src/ggv-ui.xml.h:1
msgid "About this application"
msgstr "Aplikazio honi buruz"

#: ../src/ggv-ui.xml.h:2
msgid "Cancel"
msgstr "Utzi"

#: ../src/ggv-ui.xml.h:3
msgid "Close this window"
msgstr "Leiho hau ixten du"

#: ../src/ggv-ui.xml.h:4
msgid "Help for this application"
msgstr "Aplikazio honen laguntza"

#: ../src/ggv-ui.xml.h:5
msgid "Open"
msgstr "Ireki"

#: ../src/ggv-ui.xml.h:6
msgid "Open a new PostScript document"
msgstr "PostScript dokumentu berri bat irekitzen du"

#: ../src/ggv-ui.xml.h:7
msgid "Open in new window"
msgstr "Ireki leiho berrian"

#: ../src/ggv-ui.xml.h:8
msgid "Open in this window"
msgstr "Ireki leiho honetan"

#: ../src/ggv-ui.xml.h:9
msgid "Reload"
msgstr "Berritu"

#: ../src/ggv-ui.xml.h:10
msgid "Reload current document"
msgstr "Uneko dokumentua berritzen du"

#: ../src/ggv-ui.xml.h:11
msgid "Show Side_bar"
msgstr "Erakutsi _alboko barra"

#: ../src/ggv-ui.xml.h:12
msgid "Show _Menus"
msgstr "Erakutsi _menuak"

#: ../src/ggv-ui.xml.h:13
msgid "Show _Statusbar"
msgstr "Erakut_si egoera-barra"

#: ../src/ggv-ui.xml.h:14
msgid "Show _Toolbar"
msgstr "Erakutsi _tresna-barra"

#: ../src/ggv-ui.xml.h:15
msgid "Show/hide the menus"
msgstr "Erakutsi/ezkutatu menuak"

#: ../src/ggv-ui.xml.h:16
msgid "Show/hide the sidebar"
msgstr "Erakutsi/ezkutatu alboko barra"

#: ../src/ggv-ui.xml.h:17
msgid "Show/hide the statusbar"
msgstr "Erakutsi/ezkutatu egoera-barra"

#: ../src/ggv-ui.xml.h:18
msgid "Show/hide the toolbar"
msgstr "Erakutsi/ezkutatu tresna-barra"

#: ../src/ggv-ui.xml.h:19
msgid "Toggle fullscreen mode"
msgstr "Txandakatu pantaila osoaren modua"

#: ../src/ggv-ui.xml.h:20
msgid "_About"
msgstr "H_oni buruz"

#: ../src/ggv-ui.xml.h:21
msgid "_Close"
msgstr "It_xi"

#: ../src/ggv-ui.xml.h:22
msgid "_Contents"
msgstr "_Edukia"

#: ../src/ggv-ui.xml.h:25
msgid "_Full Screen"
msgstr "_Pantaila osoan"

#: ../src/ggv-ui.xml.h:26
msgid "_Help"
msgstr "_Laguntza"

#: ../src/ggv-ui.xml.h:27
msgid "_Layout"
msgstr "_Diseinua"

#: ../src/ggv-ui.xml.h:28
msgid "_Open"
msgstr "_Ireki"

#: ../src/ggv-ui.xml.h:29
msgid "_Reload"
msgstr "_Berritu"

#: ../src/ggv-window.c:267 ../src/ggv-window.c:376 ../src/ggv-window.c:608
#: ../src/ggv-window.c:648 ../src/ggv-window.c:956
#, c-format
msgid ""
"Unable to load file:\n"
"%s"
msgstr ""
"Ezin izan da fitxategi hau kargatu:\n"
"%s"

#: ../src/ggv-window.c:303 ../src/ggv-window.c:1039
msgid "GGV: no document loaded"
msgstr "GGV: ez da dokumenturik kargatu"

#: ../src/ggv-window.c:319
msgid "Select a PostScript document"
msgstr "Hautatu Postscript dokumentu bat"

#: ../src/ggv-window.c:411
#, c-format
msgid ""
"Unable to reload file:\n"
"%s"
msgstr ""
"Ezin izan da fitxategia birkargatu:\n"
"%s"

#: ../src/ggv-window.c:522
msgid "Gary Ekker (current maintainer)"
msgstr "Gary Ekker (uneko mantentzailea)"

#: ../src/ggv-window.c:533
msgid "And many more..."
msgstr "Eta beste asko..."

#: ../src/ggv-window.c:540
msgid "translator-credits"
msgstr ""
"Iñaki Larrañaga  <dooteo@euskalgnu.org>\n"
"Hizkuntza Politikarako Sailburuordetza\n"
"<hizkpol@ej-gv.es>"

#: ../src/ggv-window.c:554
msgid "Gnome Ghostview"
msgstr "Gnome-ren Ghostview"

#: ../src/ggv-window.c:557
msgid "The GNOME PostScript document previewer"
msgstr "GNOMEren PostScript dokumentuen ikustailea"

#: ../src/ggv-window.c:982
msgid "Open recent files"
msgstr "Ireki azken fitxategiak"

#: ../src/ggv-window.c:1082
msgid "Open a file."
msgstr "Ireki fitxategia."

#: ../src/ggv-window.c:1234
msgid " - GGV"
msgstr " - GGV"

#: ../src/ggv-window.c:1325
#, c-format
msgid ""
"Unable to reload file after it changed:\n"
"%s"
msgstr ""
"Ezin izan da fitxategia birkargatu aldatu ondoren:\n"
"%s"

#: ../src/ggvutils.c:71
msgid "Portrait"
msgstr "Bertikala"

#: ../src/ggvutils.c:72
msgid "Landscape"
msgstr "Horizontala"

#: ../src/ggvutils.c:73
msgid "Upside Down"
msgstr "Buruz behera"

#: ../src/ggvutils.c:74
msgid "Seascape"
msgstr "Etzanda"

#: ../src/ggvutils.c:82
msgid "inch"
msgstr "hazbete"

#: ../src/ggvutils.c:83
msgid "mm"
msgstr "mm"

#: ../src/ggvutils.c:84
msgid "cm"
msgstr "cm"

#: ../src/ggvutils.c:85
msgid "point"
msgstr "puntu"

#: ../src/ggvutils.c:107
msgid "None"
msgstr "Bat ere ez"

#: ../src/ggvutils.c:107
msgid "Fit to page width"
msgstr "Doitu orrialde-zabalerari"

#: ../src/ggvutils.c:107
msgid "Fit to page size"
msgstr "Doitu orrialde-tamainari"

#: ../src/ggvutils.c:317
#, c-format
msgid "Unable to load ggv stock icon '%s'\n"
msgstr "Ezin da kargatu ggv-ren '%s' ikonoa\n"

#: ../src/gtkgs.c:379
msgid "No document loaded."
msgstr "Ez da dokumenturik kargatu."

#: ../src/gtkgs.c:492
msgid "File is not a valid PostScript document."
msgstr "Fitxategia ez da baliozko PostScript dokumentua."

#: ../src/gtkgs.c:1019
msgid "Broken pipe."
msgstr "Kanalizazio hautsia."

#: ../src/gtkgs.c:1208
msgid "Interpreter failed."
msgstr "Interpretatzaileak huts egin du."

#. report error
#: ../src/gtkgs.c:1312
#, c-format
msgid "Error while decompressing file %s:\n"
msgstr "Errorea %s fitxategia deskonprimatzean:\n"

#: ../src/gtkgs.c:1418
#, c-format
msgid "Error while converting pdf file %s:\n"
msgstr "Errorea %s pdf fitxategia bihurtzean:\n"

#: ../src/gtkgs.c:1744
#, c-format
msgid "Cannot open file %s.\n"
msgstr "Ezin da %s fitxategia ireki.\n"

#: ../src/gtkgs.c:1746
msgid "File is not readable."
msgstr "Fitxategia ezin da irakurri."

#: ../src/gtkgs.c:1765
#, c-format
msgid "Error while scanning file %s\n"
msgstr "Errorea %s fitxategia eskaneatzean\n"

#: ../src/gtkgs.c:1768
msgid "The file is not a PostScript document."
msgstr "Fitxategia ez da PostScript dokumentua."

#: ../src/gtkgs.c:1800
msgid "Document loaded."
msgstr "Dokumentua kargatuta."

#: ../src/main.c:75
#, c-format
msgid "Invalid geometry string \"%s\"\n"
msgstr "\"%s\" geometria-kate baliogabea\n"

#: ../src/main.c:189
msgid "Specify the number of empty windows to open."
msgstr "Zehaztu zenbat leiho huts irekiko diren."

#: ../src/main.c:190
msgid "Number of empty windows"
msgstr "Leiho hutsen kopurua"

#: ../src/main.c:192
msgid "X geometry specification (see \"X\" man page)."
msgstr "X geometria-zehaztapena (ikus eskuliburuko \"X\" man orrialdea)."

#: ../src/main.c:193
msgid "GEOMETRY"
msgstr "GEOMETRIA"

#: ../src/main.c:232
#, c-format
msgid "Failed to initialize GConf subsystem: %s"
msgstr "Huts egin du GConf azpisistema hasieratzean: %s"

#: ../src/main.c:238
msgid "Failed to initialize Bonobo!\n"
msgstr "Ezin da Bonobo hasieratu\n"

#~ msgid "A Ghostscript frontend to visualize PostScript files"
#~ msgstr "PostScript fitxategiak ikusteko Ghostscript interfazea"

#~ msgid "_Use GNOME defaults"
#~ msgstr "_Erabili GNOMEren lehenespenak"

#~ msgid "Show _both icons and text"
#~ msgstr "E_rakutsi ikonoak eta testua"

#~ msgid "Show only _icons"
#~ msgstr "Erakutsi _ikonoak soilik"

#~ msgid "Show only t_ext"
#~ msgstr "Erakutsi _testua soilik"

#~ msgid "GGV: "
#~ msgstr "GGV: "

#~ msgid "Display"
#~ msgstr "Bistaratu"

#~ msgid "Close"
#~ msgstr "Itxi"

#~ msgid "E_xit"
#~ msgstr "I_rten"

#~ msgid "Exit the program"
#~ msgstr "Programatik irteten da"

#~ msgid "New _window"
#~ msgstr "Leiho _berria"

#~ msgid "New window"
#~ msgstr "Leiho berria"

#~ msgid "Open a new window"
#~ msgstr "Leiho berria irekitzen du"

#, fuzzy
#~ msgid "Zoom to _Default"
#~ msgstr "Zooma txikiagotu"

#, fuzzy
#~ msgid "Zoom to _Fit"
#~ msgstr "Zooma txikiagotu"

#, fuzzy
#~ msgid "GGV _Preferences..."
#~ msgstr "GGVren hobespenak"

#, fuzzy
#~ msgid "Override Orientation"
#~ msgstr "Jaramonik ez dokumentuaren orientazioari"

#, fuzzy
#~ msgid "_Override Size"
#~ msgstr "_Jaramonik ez dokumentuaren tamainari"

#~ msgid "GNOME Ghostview"
#~ msgstr "GNOMEren Ghostview"

#~ msgid "Show side panel by default"
#~ msgstr "Erakutsi alboko panela lehenespenez"

#~ msgid "Show menubar by default"
#~ msgstr "Erakutsi menu-barra lehenespenez"

#~ msgid "Show toolbar by default"
#~ msgstr "Erakutsi tresna-barra lehenespenez"

#, fuzzy
#~ msgid "Show labels in toolbar"
#~ msgstr "Erakutsi _tresna-barra"

#, fuzzy
#~ msgid "Show statusbar by default"
#~ msgstr "Erakutsi menu-barra lehenespenez"

#~ msgid "Scan PDF"
#~ msgstr "Eskaneatu PDF"

#~ msgid "_Settings"
#~ msgstr "E_zarpenak"

#, fuzzy
#~ msgid ""
#~ "Ghostscript error:\n"
#~ "Probably the file is not a PostScript file."
#~ msgstr "PostScript fitxategiak ikusteko Ghostscript interfazea"

#~ msgid "Copyright (C) 1998-2002 The Free Software Foundation"
#~ msgstr "Copyright �c) 1998-2002 The Free Software Foundation"

#~ msgid "_Preferences..."
#~ msgstr "_Hobespenak..."

#~ msgid "I could not initialize Bonobo"
#~ msgstr "Ezin da Bonobo hasieratu"

#~ msgid "At end"
#~ msgstr "Amaieran"

#~ msgid "Unspecified"
#~ msgstr "Zehaztugabea"

#~ msgid "Unknown"
#~ msgstr "Ezezaguna"

#~ msgid "Encapsulated PostScript"
#~ msgstr "Postscript kapsulatua"

#~ msgid "Structured PostScript"
#~ msgstr "Postscript egituratua"

#~ msgid "Unstructured PostScript"
#~ msgstr "Postscript egituratu gabea"

#~ msgid ""
#~ "Filename: %s %s. Magnification %3d%%. Document Orientation: %s. Override "
#~ "Media: %s"
#~ msgstr ""
#~ "Fitxategi-izena: %s %s. Handitze-maila %%%3d. Dokumentuaren orientazioa: %"
#~ "s. Jaramonik ez paperari: %s"

#~ msgid "Yes"
#~ msgstr "Bai"

#~ msgid "gs warnings for %s"
#~ msgstr "%s(r)entzako gs abisuak"

#~ msgid "Opening %s..."
#~ msgstr "%s irekitzen..."

#~ msgid "Successfully loaded %s."
#~ msgstr "%s behar bezala kargatu da."

#~ msgid "Successfully saved marked pages to file %s."
#~ msgstr "Markatutako orrialdeak behar bezala gorde dira %s fitxategian."

#~ msgid "Unable to create %s."
#~ msgstr "Ezin da %s sortu."

#~ msgid "_Next Page"
#~ msgstr "_Hurrengo orrialdea"

#~ msgid "_Previous Page"
#~ msgstr "_Aurreko orrialdea"

#~ msgid "_First Page"
#~ msgstr "_Lehen orrialdea"

#~ msgid "_Last Page"
#~ msgstr "A_zken orrialdea"

#~ msgid "_Recenter Page"
#~ msgstr "_Orrialdea birzentratu"

#~ msgid "Toggle _all pages"
#~ msgstr "Marka jarri/kendu o_rrialde guztiei"

#~ msgid "Toggle o_dd pages"
#~ msgstr "Marka jarri/kendu orrialde _bakoitiei"

#~ msgid "Toggle _even pages"
#~ msgstr "Marka jarri/kendu orrialde bi_koitiei"

#~ msgid "_Unmark all pages"
#~ msgstr "Ken_du marka orrialde guztiei"

#~ msgid "Portrait orientation"
#~ msgstr "Orientazio bertikala"

#~ msgid "Landscape orientation"
#~ msgstr "Orientazio horizontala"

#~ msgid "_Upside Down"
#~ msgstr "B_uruz behera"

#~ msgid "Upside down orientation"
#~ msgstr "Buruz beherako orientazioa"

#~ msgid "Seascape orientation"
#~ msgstr "Etzandako orientazioa"

#~ msgid "_Override Document Orientation"
#~ msgstr "_Jaramonik ez dokumentuaren orientazioari"

#~ msgid "Override the orientation of the document."
#~ msgstr "Jaramonik ez dokumentuaren orientazioari."

#~ msgid "_Override Document Size"
#~ msgstr "_Jaramonik ez dokumentuaren tamainari"

#~ msgid "Override the size of page to use."
#~ msgstr "Jaramonik ez orrialdearen tamainari."

#~ msgid "Toggle menu visibility"
#~ msgstr "Menua erakutsi/ezkutatu"

#~ msgid "Toggle side panel visibility"
#~ msgstr "Alboko panela erakutsi/ezkutatu"

#~ msgid "Toggle toolbar visibility"
#~ msgstr "Tresna-barra erakutsi/ezkutatu"

#~ msgid "_Open..."
#~ msgstr "_Ireki..."

#~ msgid "Prin_t marked pages"
#~ msgstr "Inprima_tu markatutako orrialdeak"

#~ msgid "Exit GGv"
#~ msgstr "GGv-tik irteten da"

#~ msgid "Show previous page"
#~ msgstr "Erakutsi aurreko orrialdea"

#~ msgid "Show first page"
#~ msgstr "Erakutsi lehen orrialdea"

#~ msgid "Show last page"
#~ msgstr "Erakutsi azken orrialdea"

#~ msgid "Center page"
#~ msgstr "Zentratu orrialdea"

#~ msgid "Antialiasin_g"
#~ msgstr "A_ntialiasa"

#~ msgid "Toggle antialiasing"
#~ msgstr "Gaitu/Desgaitu antialiasa"

#~ msgid "_Watch File"
#~ msgstr "B_ehatu fitxategia"

#~ msgid "Updates display if file changes."
#~ msgstr "Bistaratzea eguneratzen du fitxategia aldatzen bada."

#~ msgid "Orien_tation"
#~ msgstr "Orien_tazioa"

#~ msgid "_Document"
#~ msgstr "_Dokumentua"

#~ msgid "New"
#~ msgstr "Berria"

#~ msgid "Create a new window"
#~ msgstr "Sortu leiho berria"

# without the line break the buttons of toolbar ar e tooooo wiiiiide
#~ msgid "Print marked"
#~ msgstr ""
#~ "Inprimatu\n"
#~ "markatuak"

#~ msgid "Save marked"
#~ msgstr "Gorde markatuak"

#~ msgid "Preferences"
#~ msgstr "Hobespenak"

#~ msgid "Configure the application"
#~ msgstr "Konfiguratu aplikazioa"

#~ msgid "Unmark all pages"
#~ msgstr "Kendu marka orrialde guztiei"

#~ msgid "Previous page"
#~ msgstr "Aurreko orrialdea"

#~ msgid "Zoom out"
#~ msgstr "Zooma txikiagotu"

#~ msgid "Zoom in"
#~ msgstr "Zooma handiagotu"

#~ msgid "Override media size"
#~ msgstr "Jaramonik ez paperaren tamainari"

#~ msgid "Do not use sidebar and toolbar (deprecated)"
#~ msgstr "Ez erabili alboko barra eta tresna-barra (gutxietsiak)"

#~ msgid "SPARTAN"
#~ msgstr "SPARTAN"

#~ msgid "Do not use sidebar"
#~ msgstr "Ez erabili alboko barrarik"

#~ msgid "NOSIDEBAR"
#~ msgstr "NOSIDEBAR"

#~ msgid "Do not use a menu bar"
#~ msgstr "Ez erabili menu-barrarik"

#~ msgid "NOMENUBAR"
#~ msgstr "NOMENUBAR"

#~ msgid "Do not use a tool bar"
#~ msgstr "Ez erabili tresna-barrarik"

#~ msgid "NOTOOLBAR"
#~ msgstr "NOTOOLBAR"

#~ msgid "Use the whole screen for the main window"
#~ msgstr "Erabili pantaila osoa leiho nagusirako"

#~ msgid "FULLSCREEN"
#~ msgstr "FULLSCREEN"

#~ msgid "%s does not exist."
#~ msgstr "%s ez dago."

#~ msgid ""
#~ "PostScript(TM) document viewer.\n"
#~ "Based on Tim Theisen's excellent Ghostview application."
#~ msgstr ""
#~ "PostScript(TM) dokumentu-ikustailea.\n"
#~ "Tim Theisen-en Ghostview aplikazio ezin hobean oinarritua."

#~ msgid "There are no marked pages."
#~ msgstr "Ez dago markatutako orrialderik."

#~ msgid "GGv: Save marked pages"
#~ msgstr "GGv: Gorde markatutako orrialdeak"

#~ msgid "Couldn't find the GGv manual!"
#~ msgstr "Ezin izan da GGv eskuliburua aurkitu!."
