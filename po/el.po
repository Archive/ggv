# translation of el.po to 
# translation of el.po to Greek
# GNOME ggv Greek translation.
# Copyright (C) 2000, 2001,2003, 2004, 2005 Free Software Foundation, Inc.
#
# Initial translation by Spiros.
# Simos updated to 131 messages. (lost initial count)
# simos: 145 messages, 16Feb2001, visual verification passed.
# simos: 145 messages, 19Feb2001, one got fuzzy.
# simos: 154 messages, 26Mar2001, (beasgprgmt).
# simos: 160 messages, 4Jun2001, (tengo cumple).
# kostas: updated translation, 02Jan2003 (podariko..).
# kostas: unfuzzy 1, 26Jan2003
# kostas: 28May2003, updated translation for Gnome 2.4
# kostas:12Nov2003, updates and fixes
# nikos: 29Jul2004, minor updates
# nikos: 30Aug2004, update
# Spiros Papadimitriou <spapadim+@cs.cmu.edu>, 2000.
# Simos Xenitellis <simos@hellug.gr>, 2000, 2001.
# Kostas Papadimas <pkst@gnome.org>, 2003.
# Nikos Charonitakis <charosn@her.forthnet.gr>, 2004, 2005.
msgid ""
msgstr ""
"Project-Id-Version: el\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-02-03 23:12+0200\n"
"PO-Revision-Date: 2005-02-03 23:29+0200\n"
"Last-Translator: Nikos Charonitakis <charosn@her.forthnet.gr>\n"
"Language-Team:  <team@gnome.gr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.9.1\n"
"Plural-Forms:  nplurals=2; plural=(n != 1);\n"

#: bonobo/GNOME_GGV.server.in.in.h:1
msgid "GGV PostScript Document Control"
msgstr "Έλεγχος Εγγράφου PostScript GGV"

#: bonobo/GNOME_GGV.server.in.in.h:2
msgid "GGV PostScript View"
msgstr "Προβολή PostScript GGV"

#: bonobo/GNOME_GGV.server.in.in.h:3
msgid "GGV PostScript viewer factory"
msgstr "Εργοστάσιο εμφάνισης PostScript GGV"

#: bonobo/ggv-control-ui.xml.h:2
#, no-c-format
msgid "15_0%"
msgstr "15_0%"

#: bonobo/ggv-control-ui.xml.h:3
msgid "1:1"
msgstr "1:1"

#: bonobo/ggv-control-ui.xml.h:5
#, no-c-format
msgid "25%"
msgstr "25%"

#: bonobo/ggv-control-ui.xml.h:6
msgid "Change zoom factor to default"
msgstr "Αλλαγή στην προεπιλεγμένη μεγέθυνση"

#: bonobo/ggv-control-ui.xml.h:7
msgid "Decrease zoom factor"
msgstr "Μείωση μεγέθυνσης"

#: bonobo/ggv-control-ui.xml.h:8
msgid "Fit"
msgstr "Ταίριασμα"

#: bonobo/ggv-control-ui.xml.h:9
msgid "Fit Width"
msgstr "Ταίριασμα στο Πλάτος"

#: bonobo/ggv-control-ui.xml.h:10
msgid "Fit _Width"
msgstr "Ταίριασμα στο _Πλάτος"

#: bonobo/ggv-control-ui.xml.h:11
msgid "Fit to Page _Size"
msgstr "Ταίριασμα στο _Μέγεθος Σελίδας"

#: bonobo/ggv-control-ui.xml.h:12
msgid "Fit to Page _Width"
msgstr "Ταίριασμα στο _Πλάτος Σελίδας"

#: bonobo/ggv-control-ui.xml.h:13
msgid "In"
msgstr "Μέσα"

#: bonobo/ggv-control-ui.xml.h:14
msgid "Increase zoom factor"
msgstr "Αύξηση μεγέθυνσης"

#: bonobo/ggv-control-ui.xml.h:15
msgid "Other"
msgstr "Άλλα"

#: bonobo/ggv-control-ui.xml.h:16
msgid "Out"
msgstr "Έξω"

#: bonobo/ggv-control-ui.xml.h:17
msgid "Zoom _In"
msgstr "_Μεγέθυνση"

#: bonobo/ggv-control-ui.xml.h:18
msgid "Zoom _Out"
msgstr "_Σμίκρυνση"

#: bonobo/ggv-control-ui.xml.h:19
msgid "Zoom the page to be as wide as the window"
msgstr "Μεγέθυνση της σελίδας στο πλάτος του παραθύρου"

#: bonobo/ggv-control-ui.xml.h:20
msgid "Zoom the page to fit in the window"
msgstr "Μεγέθυνση της σελίδας ώστε να ταιριάζει στο παράθυρο"

#: bonobo/ggv-control-ui.xml.h:22
#, no-c-format
msgid "_100%"
msgstr "_100%"

#: bonobo/ggv-control-ui.xml.h:24
#, no-c-format
msgid "_200%"
msgstr "_200%"

#: bonobo/ggv-control-ui.xml.h:26
#, no-c-format
msgid "_400%"
msgstr "_400%"

#: bonobo/ggv-control-ui.xml.h:28
#, no-c-format
msgid "_50%"
msgstr "_50%"

#: bonobo/ggv-control-ui.xml.h:30
#, no-c-format
msgid "_75%"
msgstr "_75%"

#: bonobo/ggv-control-ui.xml.h:31
msgid "_Auto-fit"
msgstr "_Αυτόματο ταίριασμα"

#: bonobo/ggv-control-ui.xml.h:32
msgid "_Default Zoom"
msgstr "_Προεπιλεγμένη Μεγέθυνση"

#: bonobo/ggv-control-ui.xml.h:33
msgid "_Fit Window"
msgstr "_Ταίριασμα στο Παράθυρο"

#: bonobo/ggv-control-ui.xml.h:34
msgid "_No Auto-fit"
msgstr "_Χωρίς Αυτόματο ταίριασμα"

#: bonobo/ggv-control-ui.xml.h:35
msgid "_Zoom"
msgstr "Μεγέ_θυνση"

#: bonobo/ggv-postscript-view-ui.xml.h:1 src/ggvutils.c:59
msgid "10x14"
msgstr "10x14"

#: bonobo/ggv-postscript-view-ui.xml.h:2 src/ggvutils.c:49
msgid "A0"
msgstr "A0"

#: bonobo/ggv-postscript-view-ui.xml.h:3 src/ggvutils.c:50
msgid "A1"
msgstr "A1"

#: bonobo/ggv-postscript-view-ui.xml.h:4 src/ggvutils.c:51
msgid "A2"
msgstr "A2"

#: bonobo/ggv-postscript-view-ui.xml.h:5 src/ggvutils.c:52
msgid "A3"
msgstr "A3"

#: bonobo/ggv-postscript-view-ui.xml.h:6 src/ggvutils.c:53
msgid "A4"
msgstr "A4"

#: bonobo/ggv-postscript-view-ui.xml.h:7 src/ggvutils.c:54
msgid "A5"
msgstr "A5"

#: bonobo/ggv-postscript-view-ui.xml.h:8 src/ggvutils.c:55
msgid "B4"
msgstr "B4"

#: bonobo/ggv-postscript-view-ui.xml.h:9 src/ggvutils.c:56
msgid "B5"
msgstr "B5"

#: bonobo/ggv-postscript-view-ui.xml.h:10 src/ggvutils.c:42
msgid "BBox"
msgstr "BBox"

#: bonobo/ggv-postscript-view-ui.xml.h:11 src/ggvutils.c:48
msgid "Executive"
msgstr "Executive"

#: bonobo/ggv-postscript-view-ui.xml.h:12
msgid "First"
msgstr "Πρώτη"

#: bonobo/ggv-postscript-view-ui.xml.h:13 src/ggvutils.c:57
msgid "Folio"
msgstr "Folio"

#: bonobo/ggv-postscript-view-ui.xml.h:14
msgid "Go to first page"
msgstr "Μετάβαση στην πρώτη σελίδα"

#: bonobo/ggv-postscript-view-ui.xml.h:15
msgid "Go to last page"
msgstr "Μετάβαση στην τελευταία σελίδα"

#: bonobo/ggv-postscript-view-ui.xml.h:16
msgid "Go to next page"
msgstr "Μετάβαση στην επόμενη σελίδα"

#: bonobo/ggv-postscript-view-ui.xml.h:17
msgid "Go to previous page"
msgstr "Μετάβαση στην προηγούμενη σελίδα"

#: bonobo/ggv-postscript-view-ui.xml.h:18
msgid "Last"
msgstr "Τελευταία"

#: bonobo/ggv-postscript-view-ui.xml.h:19 src/ggvutils.c:45
msgid "Ledger"
msgstr "Ledger"

#: bonobo/ggv-postscript-view-ui.xml.h:20 src/ggvutils.c:46
msgid "Legal"
msgstr "Legal"

#: bonobo/ggv-postscript-view-ui.xml.h:21 src/ggvutils.c:43
msgid "Letter"
msgstr "Letter"

#: bonobo/ggv-postscript-view-ui.xml.h:22
msgid "Next"
msgstr "Επόμενη"

#: bonobo/ggv-postscript-view-ui.xml.h:23
msgid "Open the preferences dialog"
msgstr "Άνοιγμα του διαλόγου προτιμήσεων"

#: bonobo/ggv-postscript-view-ui.xml.h:24
msgid "Post_Script Viewer Preferences"
msgstr "Προτιμήσεις Εφαρμογής Προβολής Post_Script GGV"

#: bonobo/ggv-postscript-view-ui.xml.h:25
msgid "Prev"
msgstr "Προηγ"

#: bonobo/ggv-postscript-view-ui.xml.h:26
msgid "Print"
msgstr "Εκτύπωση"

#: bonobo/ggv-postscript-view-ui.xml.h:27
msgid "Print marked pages"
msgstr "Εκτύπωση σημαδεμένων σελίδων"

#: bonobo/ggv-postscript-view-ui.xml.h:28
msgid "Print the whole document"
msgstr "Εκτύπωση ολόκληρου του εγγράφου"

#: bonobo/ggv-postscript-view-ui.xml.h:29 src/ggvutils.c:58
msgid "Quarto"
msgstr "Quarto"

#: bonobo/ggv-postscript-view-ui.xml.h:30
msgid "Save marked pages to a file"
msgstr "Αποθήκευση σημαδεμένων σελίδων σε αρχείο"

#: bonobo/ggv-postscript-view-ui.xml.h:31 src/ggvutils.c:47
msgid "Statement"
msgstr "Statement"

#: bonobo/ggv-postscript-view-ui.xml.h:32 src/ggvutils.c:44
msgid "Tabloid"
msgstr "Ταμπλόϊντ"

#: bonobo/ggv-postscript-view-ui.xml.h:33
msgid "_Document Orientation"
msgstr "_Προσανατολισμός Εγγράφου"

#: bonobo/ggv-postscript-view-ui.xml.h:34
msgid "_Document Size"
msgstr "Μέγεθος _Έγγράφου"

#: bonobo/ggv-postscript-view-ui.xml.h:35 src/ggv-ui.xml.h:23
msgid "_Edit"
msgstr "_Επεξεργασία"

#: bonobo/ggv-postscript-view-ui.xml.h:36 src/ggv-ui.xml.h:24
msgid "_File"
msgstr "_Αρχείο"

#: bonobo/ggv-postscript-view-ui.xml.h:37
msgid "_First page"
msgstr "_Πρώτη σελίδα"

#: bonobo/ggv-postscript-view-ui.xml.h:38
msgid "_Go"
msgstr "_Μετάβαση"

#: bonobo/ggv-postscript-view-ui.xml.h:39
msgid "_Landscape"
msgstr "_Τοπίο"

#: bonobo/ggv-postscript-view-ui.xml.h:40
msgid "_Last page"
msgstr "_Τελευταία σελίδα"

#: bonobo/ggv-postscript-view-ui.xml.h:41
msgid "_Next page"
msgstr "Επόμε_νη σελίδα"

#: bonobo/ggv-postscript-view-ui.xml.h:42
msgid "_Orientation"
msgstr "Πρ_οσανατολισμός"

# Not sure how the terms "portrait" and "landscape" are usually
# translated; if anyone knows what the term in the Greek version
# of MSWindoze is and if it is better (or equally good), maybe we
# would want to use that for consistency and for the sake of newbies?
#: bonobo/ggv-postscript-view-ui.xml.h:43
msgid "_Portrait"
msgstr "_Κατακόρυφα"

#: bonobo/ggv-postscript-view-ui.xml.h:44
msgid "_Previous page"
msgstr "_Προηγούμενη σελίδα"

#: bonobo/ggv-postscript-view-ui.xml.h:45
msgid "_Print document"
msgstr "Εκτύ_πωση εγγράφου"

#: bonobo/ggv-postscript-view-ui.xml.h:46
msgid "_Print marked pages"
msgstr "Εκτύ_πωση σημαδεμένων σελίδων"

#: bonobo/ggv-postscript-view-ui.xml.h:47
msgid "_Save marked pages"
msgstr "Αποθήκευ_ση σημαδεμένων σελίδων..."

#: bonobo/ggv-postscript-view-ui.xml.h:48
msgid "_Seascape"
msgstr "Ανάπο_δο τοπίο"

#: bonobo/ggv-postscript-view-ui.xml.h:49
msgid "_Size"
msgstr "_Μέγεθος"

#: bonobo/ggv-postscript-view-ui.xml.h:50
msgid "_Upside down"
msgstr "Ανάπο_δα"

#: bonobo/ggv-postscript-view-ui.xml.h:51 src/ggv-ui.xml.h:30
msgid "_View"
msgstr "_Προβολή"

#: bonobo/ggv-postscript-view.c:874
#, c-format
msgid ""
"Unable to execute print command:\n"
"%s"
msgstr ""
"Αδυναμία εκτέλεσης εντολής εκτύπωσης:\n"
"%s"

#: bonobo/ggv-postscript-view.c:931
msgid ""
"No pages have been marked.\n"
"Do you want to save the whole document?"
msgstr ""
"Δεν υπάρχουν σημαδεμένες σελίδες.\n"
"Θέλετε να αποθηκεύσετε όλο το έγγραφο;"

#: bonobo/ggv-postscript-view.c:956
msgid "Select a file to save pages as"
msgstr "Επιλέξτε ένα αρχείο για την αποθήκευση σελίδων ως"

#: bonobo/ggv-postscript-view.c:964 src/ggv-window.c:330
msgid "PostScript Documents"
msgstr "Έγγραφα Postscript"

#: bonobo/ggv-postscript-view.c:970 src/ggv-window.c:336
msgid "All Files"
msgstr "Όλα τα Αρχεία"

#: bonobo/ggv-postscript-view.c:1009
msgid ""
"A file with this name already exists.\n"
"Do you want to overwrite it?"
msgstr ""
"Ένα αρχείο με αυτό το όνομα υπάρχει ήδη.\n"
"Να αντικατασταθεί;"

#: bonobo/ggv-postscript-view.c:1050
msgid ""
"No pages have been marked.\n"
"Do you want to print the whole document?"
msgstr ""
"Δεν υπάρχουν σημαδεμένες σελίδες.\n"
"Θέλετε να εκτυπώσετε όλο το έγγραφο;"

#: bonobo/ggv-postscript-view.c:2117
msgid "GhostScript output"
msgstr "Αποτέλεσμα Ghostscript"

#: bonobo/ggv-postscript-view.c:2149
msgid "Document title"
msgstr "Τίτλος εγγράφου"

#: bonobo/ggv-postscript-view.c:2152
msgid "GGV control status"
msgstr "Κατάσταση ελέγχου GGV"

#: bonobo/ggv-postscript-view.c:2155
msgid "Number of pages"
msgstr "Αριθμός σελίδων"

#: bonobo/ggv-postscript-view.c:2158
msgid "Page names"
msgstr "Ονόματα σελίδας"

#: bonobo/ggv-postscript-view.c:2161
msgid "Current page number"
msgstr "Τρέχων αριθμός σελίδας"

#: bonobo/ggv-postscript-view.c:2164
msgid "Document width"
msgstr "Πλάτος εγγράφου"

#: bonobo/ggv-postscript-view.c:2167
msgid "Document height"
msgstr "Ύψος εγγράφου"

#: bonobo/ggv-postscript-view.c:2171
msgid "Document orientation"
msgstr "Προσανατολισμός εγγράφου"

#: bonobo/ggv-postscript-view.c:2174
msgid "Default orientation"
msgstr "Προεπιλεγμένος προσανατολισμός"

#: bonobo/ggv-postscript-view.c:2178
msgid "Default size"
msgstr "Προεπιλεγμένο μέγεθος"

#: bonobo/ggv-postscript-view.c:2183
msgid "Override document orientation"
msgstr "Αγνόηση προσανατολισμού από έγγραφο"

#: bonobo/ggv-postscript-view.c:2187
msgid "Override document size"
msgstr "Παράκαμψη μεγέθους εγγράφου"

#: bonobo/ggv-postscript-view.c:2191 ggv.schemas.in.h:21
msgid "Respect EOF comment"
msgstr "Διατήρηση σχολίων τέλους αρχείου"

#: bonobo/ggv-postscript-view.c:2195
msgid "Antialiasing"
msgstr "Εξομάλυνση"

#: bonobo/ggv-sidebar.c:358
msgid "Toggle marked state of all pages"
msgstr "Εναλλαγή κατάστασης σημείωσης όλων των σελίδων"

#: bonobo/ggv-sidebar.c:359
msgid ""
"Toggle marked state of all pages: previously marked pages will be unmarked "
"and unmarked ones will become marked."
msgstr ""
"Εναλλαγή κατάστασης σημείωσης όλων των σελίδων: οι σελίδες που έχουν "
"σημειωθεί προηγουμένως θα αποσημειωθούν ενώ θα σημειωθούν οι μη σημειωμένες."

#: bonobo/ggv-sidebar.c:371
msgid "Toggle marked state of odd pages"
msgstr "Εναλλαγή κατάστασης σημείωσης των περιττών σελίδων"

#: bonobo/ggv-sidebar.c:372
msgid ""
"Toggle marked state of odd pages: previously marked odd pages will be "
"unmarked and unmarked ones will become marked."
msgstr ""
"Εναλλαγή κατάστασης σημείωσης των περιττών σελίδων: οι περιττές σελίδες που "
"έχουν σημειωθεί προηγουμένως θα αποσημειωθούν ενώ θα σημειωθούν οι μη "
"σημειωμένες."

#: bonobo/ggv-sidebar.c:386
msgid "Toggle marked state of even pages"
msgstr "Εναλλαγή κατάστασης σημείωσης των άρτιων σελίδων."

#: bonobo/ggv-sidebar.c:387
msgid ""
"Toggle marked state of even pages: previously marked even pages will be "
"unmarked and unmarked ones will become marked."
msgstr ""
"Εναλλαγή κατάστασης σημείωσης των άρτιων σελίδων.οι άρτιες σελίδες που έχουν "
"σημειωθεί προηγουμένως θα αποσημειωθούν ενώ θα σημειωθούν οι μη σημειωμένες."

#: bonobo/ggv-sidebar.c:401
msgid "Clear marked state of all pages"
msgstr "Εκκαθάριση κατάστασης σημείωσης όλων των σελίδων"

#: bonobo/ggv-sidebar.c:402
msgid "Clear marked state of all pages: all pages will be unmarked."
msgstr ""
"Εκκαθάριση κατάστασης σημείωσης όλων των σελίδων: όλες οι σελίδες θα "
"αποσημειωθούν."

#: ggv.desktop.in.h:1
msgid "PostScript Viewer"
msgstr "Εφαρμογή προβολής PostScript"

#: ggv.desktop.in.h:2
msgid "View PostScript files"
msgstr "Προβολή αρχείων PostScript"

#: ggv.schemas.in.h:1
msgid "Antialiased rendering"
msgstr "Εξομάλυνση"

#: ggv.schemas.in.h:2
msgid "Antialiasing arguments"
msgstr "Παράμετροι εξομάλυνσης"

#: ggv.schemas.in.h:3
msgid "Autojump"
msgstr "Αυτόματη μεταπήδηση"

#: ggv.schemas.in.h:4
msgid "Command line arguments for antialiasing."
msgstr "Ορίσματα γραμμής εντολών διεργασίας για εξομάλυνση."

#: ggv.schemas.in.h:5
msgid "Command line used to execute the PostScript interpreter."
msgstr ""
"Γραμμή εντολών που χρησιμοποιείται για την εκτέλεση του PostScript "
"interpreter."

#: ggv.schemas.in.h:6
msgid "Coordinate units"
msgstr "Μονάδες συντεταγμένων"

#: ggv.schemas.in.h:7
msgid "Default document orientation"
msgstr "Προεπιλεγμένος προσανατολισμός εγγράφου"

#: ggv.schemas.in.h:8
msgid "Default page size"
msgstr "Προεπιλεγμένο μέγεθος σελίδας"

#: ggv.schemas.in.h:9
msgid "Default zoom factor"
msgstr "Προεπιλεγμένος συντελεστής μεγέθυνσης"

#: ggv.schemas.in.h:10
msgid ""
"If true, the orientation specified by the document will be overriden with "
"the default orientation."
msgstr ""
"Αν true, ο προσανατολισμός που καθορίζεται από το έγγραφο θα παρακάμπτεται "
"με τον προεπιλεγμένο προσανατολισμό."

#: ggv.schemas.in.h:11
msgid "Override document specified orientation"
msgstr "Αγνόηση προσανατολισμού από έγγραφο"

#: ggv.schemas.in.h:12
msgid "Override document specified page size"
msgstr "Παράκαμψη μεγέθους σελίδας εγγράφου"

#: ggv.schemas.in.h:13
msgid "PDF to DSC conversion command"
msgstr "Εντολή μετατροπής PDF σε DSC "

#: ggv.schemas.in.h:14
msgid "PDF to PS conversion command"
msgstr "Εντολή μετατροπής PDF σε PS"

#: ggv.schemas.in.h:15
msgid "Page flip"
msgstr "Γύρισμα σελίδας"

#: ggv.schemas.in.h:16
msgid "Panel on right-hand side"
msgstr "Πλευρικό ταμπλό στο δεξιό μέρος"

#: ggv.schemas.in.h:17
msgid "Percentage of the viewable area to move when scrolling."
msgstr "Ποσοστό του ορατού μέρους για μετακίνηση κατά την κύλιση."

#: ggv.schemas.in.h:18
msgid "Percentage of the viewable area to scroll"
msgstr "Ποσοστό της ορατής περιοχής για κύλιση"

#: ggv.schemas.in.h:19
msgid "PostScript interpreter command"
msgstr "Εντολή PostScript interpreter "

#: ggv.schemas.in.h:20
msgid "Printing command"
msgstr "Εντολή εκτύπωσης"

#: ggv.schemas.in.h:22
msgid "Save window geometry"
msgstr "Αποθήκευση γεωμετρίας παραθύρου"

#: ggv.schemas.in.h:23
msgid "Show menubar"
msgstr "Προβολή μπάρας μενού"

#: ggv.schemas.in.h:24
msgid "Show previous visible area when scrolling"
msgstr "Εμφάνιση του τελευταίου ορατού μέρους κατά την κύλιση"

#: ggv.schemas.in.h:25
msgid "Show side panel"
msgstr "Προβολή πλευρικού ταμπλό"

#: ggv.schemas.in.h:26
msgid "Show statusbar"
msgstr "Προβολή γραμμής κατάστασης"

#: ggv.schemas.in.h:27
msgid "Show toolbar"
msgstr "Προβολή εργαλειοθήκης"

#: ggv.schemas.in.h:28
msgid "The command line used to convert PDF files to PostScript."
msgstr ""
"Η γραμμή εντολής που χρησιμοποιείται για τη μετατροπή αρχείων PDF σε "
"PostScript."

#: ggv.schemas.in.h:29
msgid "The command line used to create PostScript DSC files from PDF files."
msgstr ""
"Η γραμμή εντολής που χρησιμοποιείται για τη δημιουργία αρχείων PostScript "
"DSC από PDF."

#: ggv.schemas.in.h:30
msgid "The command used to unpack files compressed with bzip2."
msgstr "Η εντολή που χρησιμοποιείται για την αποσυμπίεση αρχείων bzip2."

#: ggv.schemas.in.h:31
msgid "The command used to unpack files compressed with gzip."
msgstr "Η εντολή που χρησιμοποιείται για την αποσυμπίεση αρχείων gzip."

#: ggv.schemas.in.h:32
msgid ""
"The default page size to use when no page size is specified by the document "
"or when the user wishes to override the size specified in the document."
msgstr ""
"Το προεπιλεγμένο μέγεθος σελίδας που θα χρησιμοποιείται όταν δεν έχει "
"καθοριστεί μέγεθος από το έγγραφο ή όταν ο χρήστης επιθυμεί να παρακάμψει το "
"μέγεθος που έχει καθοριστεί από το έγγραφο."

#: ggv.schemas.in.h:33
msgid ""
"The default zoom factor for the document. The value should belong to the "
"interval [0.05, 30.0]."
msgstr ""
"Ο προεπιλεγμένος συντελεστής μεγέθυνσης για το έγγραφο. Η τιμή θα πρέπει να "
"ανήκει στο διάστημα [0.05, 30.0]."

#: ggv.schemas.in.h:34
msgid "The height of the viewer's window."
msgstr "Το ύψος του παραθύρου προβολής."

#: ggv.schemas.in.h:35
msgid ""
"The orientation to use when the document defines no orientation or when the "
"user wishes to override the document specified orientation."
msgstr ""
"Ο προσανατολισμός που θα χρησιμοποιείται όταν δεν έχει καθοριστεί "
"προσανατολισμός από το έγγραφο ή όταν ο χρήστης επιθυμεί να παρακάμψει τον "
"προσανατολισμό που έχει καθοριστεί από το έγγραφο."

#: ggv.schemas.in.h:36
msgid "The shell command used to print files."
msgstr "Η εντολή φλοιού που χρησιμοποιείται για την εκτύπωση αρχείων."

#: ggv.schemas.in.h:37
msgid "The width of the viewer's window."
msgstr "Το πλάτος του παραθύρου προβολής."

#: ggv.schemas.in.h:38
msgid "Toolbar style"
msgstr "Στυλ εργαλειοθήκης"

#: ggv.schemas.in.h:39
msgid "Unbzip2 command"
msgstr "Εντολή Unbzip2"

#: ggv.schemas.in.h:40
msgid "Ungzip command"
msgstr "Εντολή Ungzip"

#: ggv.schemas.in.h:41
msgid "Units for the coordinate display."
msgstr "Μονάδες για τη συντεταγμένη εμφάνιση."

#: ggv.schemas.in.h:42
msgid "Viewer window height"
msgstr "Ύψος παραθύρου προβολής"

#: ggv.schemas.in.h:43
msgid "Viewer window width"
msgstr "Πλάτος παραθύρου προβολής"

#: ggv.schemas.in.h:44
msgid "Watch document for changes"
msgstr "Παρακολούθηση εγγράφου για αλλαγές"

#: ggv.schemas.in.h:45
msgid "Whether GGV should automatically flip pages when you scroll to the edge."
msgstr "Αν το GGV θα γυρίζει αυτόματα σελίδες κατά την κύλιση στις άκρες."

#: ggv.schemas.in.h:46
msgid ""
"Whether the PostScript display widget should poll the viewed file for "
"changes and reload it if it changes."
msgstr ""
"Αν το γραφικό συστατικό προβολής PostScript θα ελέγχει το προβαλλόμενο "
"αρχείο για αλλαγές και το επαναφορτώνει αν αλλάζει."

#: ggv.schemas.in.h:47
msgid ""
"Whether the PostScript display widget should use its default page size "
"instead of the one specified in the document."
msgstr ""
"Αν το γραφικό συστατικό προβολής PostScript θα χρησιμοποιεί το προεπιλεγμένο "
"μέγεθος σελίδας αντί για αυτό που καθορίζεται από το έγγραφο."

#: ggv.schemas.in.h:48
msgid ""
"Whether the PostScript display widget will respect the End Of File comment. "
"If true, the widget will only parse the document until EOF comment."
msgstr ""
"Ειδοποιεί αν το γραφικό συστατικό προβολής PostScript θα υπακούει στο σχόλιο "
"End Of File. Αν true, το γραφικό συστατικό θα αναλύει το έγγραφο μόνο μεχρι "
"το σχόλιο EOF."

#: ggv.schemas.in.h:49
msgid "Whether the control should jump to next page when using keyboard to scroll."
msgstr ""
"Αν ο έλεγχος θα πρέπει να μεταβαίνει στην επόμενη σελίδα κατά τη χρήση "
"πληκτρολογίου για την κύλιση."

#: ggv.schemas.in.h:50
msgid "Whether the document should be rendered in an antialiased mode."
msgstr "Αν το έγγραφο θα προβάλλεται σε λειτουργία εξομάλυνσης."

#: ggv.schemas.in.h:51
msgid "Whether the menubar should be visible by default."
msgstr "Αν η εργαλειοθήκη θα είναι προεπιλεγμένα ορατή."

#: ggv.schemas.in.h:52
msgid "Whether the side panel should appear on the right of the display area."
msgstr "Αν το πλευρικό ταμπλό θα εμφανίζεται στα δεξιά της περιοχής προβολής."

#: ggv.schemas.in.h:53
msgid "Whether the side panel should be visible by default."
msgstr "Αν το πλευρικό ταμπλό θα είναι προεπιλεγμένα ορατό."

#: ggv.schemas.in.h:54
msgid "Whether the statusbar should be visible by default."
msgstr "Αν η γραμμή κατάστασης θα είναι προεπιλεγμένα ορατή."

#: ggv.schemas.in.h:55
msgid ""
"Whether the toolbar buttons show icons, text, both or use the GNOME-wide "
"default."
msgstr ""
"Αν τα κουμπιά της εργαλειοθήκης θα προβάλλουν εικονίδια, κείμενο , και τα "
"δύο ή θα χρησιμοποιούν τα προεπιλεγμένα του Gnome."

#: ggv.schemas.in.h:56
msgid "Whether the toolbar should be visible by default."
msgstr "Αν η εργαλειοθήκη θα είναι προεπιλεγμένα ορατή."

#: ggv.schemas.in.h:57
msgid ""
"Whether the user wants to see a rectangle outlining the previously visible "
"area when scrolling."
msgstr ""
"Αν ο χρήστης επιθυμεί να βλέπει μια επισήμανση της προηγούμενα ορατής "
"περιοχής κατά την κύλιση."

#: ggv.schemas.in.h:58
msgid "Whether the viewer should save and restore the window geometry."
msgstr ""
"Αν το πρόγραμμα προβολής θα πρέπει να αποθηκεύει και να επαναφέρει την "
"γεωμετρία του παραθύρου."

#: src/ggv-prefs-ui.c:541
msgid "GGV Preferences"
msgstr "Προτιμήσεις GGV"

#. zoom choice menu
#: src/ggv-prefs-ui.c:547
msgid "Default _Zoom:"
msgstr "Προεπιλεγμένη _Μεγέθυνση:"

#. auto-fit choice menu
#: src/ggv-prefs-ui.c:575
msgid "A_uto-fit mode:"
msgstr "Λειτουργία α_υτόματου ταιριάσματος:"

#. units choice menu
#: src/ggv-prefs-ui.c:600
msgid "Coordinate _units:"
msgstr "_Μονάδες συντεταγμένων:"

# The translation for "fallback" sucks completely!
#. size choice menu
#: src/ggv-prefs-ui.c:624
msgid "_Fallback page size:"
msgstr "Μέγεθος χαρτιού προσ_φυγής:"

#: src/ggv-prefs-ui.c:647
msgid "Override _document size"
msgstr "Παράκαμψη μεγέθους ε_γγράφου"

#. orientation choice menu
#: src/ggv-prefs-ui.c:656
msgid "Fallback _media orientation:"
msgstr "Προσανατολισμός _μέσου προσφυγής:"

#: src/ggv-prefs-ui.c:680
msgid "O_verride document orientation"
msgstr "Πα_ράκαμψη προσανατολισμού εγγράφου"

#. antialiasing
#: src/ggv-prefs-ui.c:689
msgid "A_ntialiasing"
msgstr "Εξομάλυ_νση"

#: src/ggv-prefs-ui.c:700
msgid "_Respect EOF comments"
msgstr "Διατή_ρηση σχολίων τέλους αρχείου"

#. watch file
#: src/ggv-prefs-ui.c:709
msgid "_Watch file"
msgstr "Παρακολού_θηση αρχείου"

#: src/ggv-prefs-ui.c:718
msgid "Document"
msgstr "Έγγραφο"

#. show side panel
#: src/ggv-prefs-ui.c:727
msgid "_Show side panel"
msgstr "Εμφάνι_ση πλευρικού ταμπλό"

#: src/ggv-prefs-ui.c:738
msgid "_Put side panel on the right-hand side"
msgstr "Το_ποθέτηση πλευρικού ταμπλό στη δεξιά μεριά"

#. show menubar
#: src/ggv-prefs-ui.c:747
msgid "Show _menubar"
msgstr "Εμφάνιση _μενού"

#. show toolbar
#: src/ggv-prefs-ui.c:756
msgid "Show _toolbar"
msgstr "Εμφάνιση _εργαλειοθήκης"

#. show statusbar
#: src/ggv-prefs-ui.c:766
msgid "Show statusba_r"
msgstr "Εμφάνιση γραμμής κα_τάστασης"

#. save geometry
#: src/ggv-prefs-ui.c:776
msgid "Save _geometry"
msgstr "Αποθήκευση _γεωμετρίας"

#: src/ggv-prefs-ui.c:785
msgid "Layout"
msgstr "Διάταξη"

#: src/ggv-prefs-ui.c:796
msgid "_Jump to beginning of page"
msgstr "_Μετάβαση στην αρχή σελίδας"

#: src/ggv-prefs-ui.c:805
msgid "Automatically _flip pages"
msgstr "Αυτόματο _flip σελίδων"

#: src/ggv-prefs-ui.c:816
msgid "Outline _last visible part when scrolling"
msgstr "Σημείωση τε_λευταίου ορατού μέρους κατά την κύλιση"

#: src/ggv-prefs-ui.c:827
msgid "Amount of _visible area to scroll"
msgstr "Μέγεθος της _ορατής περιοχής για κύλιση"

#: src/ggv-prefs-ui.c:840
msgid "Navigation"
msgstr "Πλοήγηση"

#. interpreter
#: src/ggv-prefs-ui.c:849
msgid "_Interpreter:"
msgstr "_Διερμηνέας:"

#. antialiasing
#: src/ggv-prefs-ui.c:869
msgid "A_ntialiasing:"
msgstr "Εξομάλυ_νση:"

#. PDF->DSC command
#: src/ggv-prefs-ui.c:889
msgid "Convert _PDF to DSC file:"
msgstr "Μετατροπή _PDF σε αρχείο DSC:"

#. scan PDF command
#: src/ggv-prefs-ui.c:913
msgid "Convert _PDF to PS:"
msgstr "Μετατροπή _PDF σε PS:"

#. unzip command: gzip
#: src/ggv-prefs-ui.c:937
msgid "_Gzip:"
msgstr "_Gzip:"

#. unzip command: bzip2
#: src/ggv-prefs-ui.c:957
msgid "_Bzip2:"
msgstr "_Bzip2:"

#: src/ggv-prefs-ui.c:976
msgid "Ghostscript"
msgstr "Ghostscript"

#. print command
#: src/ggv-prefs-ui.c:985
msgid "_Print command:"
msgstr "Εντολή εκτύ_πωσης:"

#: src/ggv-prefs-ui.c:1003
msgid "Printing"
msgstr "Εκτύπωση"

#: src/ggv-ui.xml.h:1
msgid "About this application"
msgstr "Περί αυτής της εφαρμογής"

#: src/ggv-ui.xml.h:2
msgid "Cancel"
msgstr "Ακύρωση"

#: src/ggv-ui.xml.h:3
msgid "Close this window"
msgstr "Κλείσιμο αυτού του παραθύρου"

#: src/ggv-ui.xml.h:4
msgid "Help for this application"
msgstr "Βοήθεια για αυτήν την εφαρμογή"

#: src/ggv-ui.xml.h:5
msgid "Open"
msgstr "Άνοιγμα"

#: src/ggv-ui.xml.h:6
msgid "Open a new PostScript document"
msgstr "Άνοιιγμα ενός νέου εγγράφου Postscript"

#: src/ggv-ui.xml.h:7
msgid "Open in new window"
msgstr "Άνοιγμα σε νέο παράθυρο"

#: src/ggv-ui.xml.h:8
msgid "Open in this window"
msgstr "Άνοιγμα σε αυτό το παράθυρο"

#: src/ggv-ui.xml.h:9
msgid "Reload"
msgstr "Επαναφόρτωση"

#: src/ggv-ui.xml.h:10
msgid "Reload current document"
msgstr "Ανανέωση τρέχοντος εγγράφου"

#: src/ggv-ui.xml.h:11
msgid "Show Side_bar"
msgstr "Εμφάνιση _Εργαλειοθήκης"

#: src/ggv-ui.xml.h:12
msgid "Show _Menus"
msgstr "Εμφάνιση _Μενού"

#: src/ggv-ui.xml.h:13
msgid "Show _Statusbar"
msgstr "Εμφάνιση _Γραμμής κατάστασης"

#: src/ggv-ui.xml.h:14
msgid "Show _Toolbar"
msgstr "Εμφάνιση _Εργαλειοθήκης"

#: src/ggv-ui.xml.h:15
msgid "Show/hide the menus"
msgstr "Προβολή/απόκρυψη των μενού"

#: src/ggv-ui.xml.h:16
msgid "Show/hide the sidebar"
msgstr "Προβολή/απόκρυψη του πλευρικού ταμπλό"

#: src/ggv-ui.xml.h:17
msgid "Show/hide the statusbar"
msgstr "Προβολή/απόκρυψη γραμμής κατάστασης"

#: src/ggv-ui.xml.h:18
msgid "Show/hide the toolbar"
msgstr "Προβολή/απόκρυψη της εργαλειοθήκης"

# Need a better (more precise/descriptive) term for "toggle" ...
#: src/ggv-ui.xml.h:19
msgid "Toggle fullscreen mode"
msgstr "Εναλλαγή λειτουργίας πλήρους οθόνης"

#: src/ggv-ui.xml.h:20
msgid "_About"
msgstr "_Περί"

#: src/ggv-ui.xml.h:21
msgid "_Close"
msgstr "_Κλείσιμο"

#: src/ggv-ui.xml.h:22
msgid "_Contents"
msgstr "Περιε_χόμενα"

#: src/ggv-ui.xml.h:25
msgid "_Full Screen"
msgstr "_Πλήρης Οθόνη"

#: src/ggv-ui.xml.h:26
msgid "_Help"
msgstr "_Βοήθεια"

#: src/ggv-ui.xml.h:27
msgid "_Layout"
msgstr "_Διάταξη"

#: src/ggv-ui.xml.h:28
msgid "_Open"
msgstr "_Άνοιγμα"

#: src/ggv-ui.xml.h:29
msgid "_Reload"
msgstr "Επα_ναφόρτωση"

#: src/ggv-window.c:267 src/ggv-window.c:376 src/ggv-window.c:607
#: src/ggv-window.c:647 src/ggv-window.c:955
#, c-format
msgid ""
"Unable to load file:\n"
"%s"
msgstr ""
"Αδυναμία φόρτωσης αρχείου:\n"
"%s"

#: src/ggv-window.c:303 src/ggv-window.c:1038
msgid "GGV: no document loaded"
msgstr "GGV: δε φορτώθηκε έγγραφο"

#: src/ggv-window.c:319
msgid "Select a PostScript document"
msgstr "Επιλογή ενός εγγράφου Postscript"

#: src/ggv-window.c:411
#, c-format
msgid ""
"Unable to reload file:\n"
"%s"
msgstr ""
"Αδυναμία επαναφόρτωσης αρχείου:\n"
"%s"

#: src/ggv-window.c:522
msgid "Jaka Mocnik (current maintainer)"
msgstr "Jaka Mocnik (current maintainer)"

#: src/ggv-window.c:532
msgid "And many more..."
msgstr "Και πολλοί άλλοι..."

#: src/ggv-window.c:539
msgid "translator-credits"
msgstr ""
"Spiros Papadimitriou <spapadim+@cs.cmu.edu>\n"
" Simos Xenitellis <simos@hellug.gr>\n"
" Kostas Papadimas <pkst@gmx.net>"

#: src/ggv-window.c:553
msgid "Gnome Ghostview"
msgstr "Gnome Ghostview"

#: src/ggv-window.c:556
msgid "The GNOME PostScript document previewer"
msgstr "Εφαρμογή προεπισκόπησης εγγράφου PostScript του GNOME"

#: src/ggv-window.c:981
msgid "Open recent files"
msgstr "Άνοιγμα πρόσφατων αρχείων"

#: src/ggv-window.c:1081
msgid "Open a file."
msgstr "Άνοιγμα αρχείου."

#: src/ggv-window.c:1233
msgid " - GGV"
msgstr " - GGV"

#: src/ggv-window.c:1324
#, c-format
msgid ""
"Unable to reload file after it changed:\n"
"%s"
msgstr ""
"Αδυναμία επαναφόρτωσης αρχείου μετά την τροποποίηση του:\n"
"%s"

# Not sure how the terms "portrait" and "landscape" are usually
# translated; if anyone knows what the term in the Greek version
# of MSWindoze is and if it is better (or equally good), maybe we
# would want to use that for consistency and for the sake of newbies?
#: src/ggvutils.c:71
msgid "Portrait"
msgstr "Κατακόρυφα"

#: src/ggvutils.c:72
msgid "Landscape"
msgstr "Οριζόντια"

#: src/ggvutils.c:73
msgid "Upside Down"
msgstr "Ανάποδα"

#: src/ggvutils.c:74
msgid "Seascape"
msgstr "Tοπίο"

#: src/ggvutils.c:82
msgid "inch"
msgstr "ίντσα"

#: src/ggvutils.c:83
msgid "mm"
msgstr "mm"

#: src/ggvutils.c:84
msgid "cm"
msgstr "cm"

#: src/ggvutils.c:85
msgid "point"
msgstr "στιγμή"

#: src/ggvutils.c:107
msgid "None"
msgstr "Κανένα"

#: src/ggvutils.c:107
msgid "Fit to page width"
msgstr "Ταίριασμα στο πλάτος της σελίδας"

#: src/ggvutils.c:107
msgid "Fit to page size"
msgstr "Ταίριασμα στο μέγεθος της σελίδας"

#: src/ggvutils.c:317
#, c-format
msgid "Unable to load ggv stock icon '%s'\n"
msgstr "Αδυναμία φόρτωσης ggv stock icon '%s'\n"

#: src/gtkgs.c:379
msgid "No document loaded."
msgstr "Δε φορτώθηκε έγγραφο."

#: src/gtkgs.c:492
msgid "File is not a valid PostScript document."
msgstr "Το αρχείο δεν είναι ένα έγκυρο έγγραφο Postscript."

#: src/gtkgs.c:1019
msgid "Broken pipe."
msgstr "Διακοπείσα σωλήνωση."

#: src/gtkgs.c:1208
msgid "Interpreter failed."
msgstr "Αποτυχία διερμηνέα."

#. report error
#: src/gtkgs.c:1312
#, c-format
msgid "Error while decompressing file %s:\n"
msgstr "Σφάλμα κατά την αποσυμπίεση του αρχείου %s:\n"

#: src/gtkgs.c:1418
#, c-format
msgid "Error while converting pdf file %s:\n"
msgstr "Σφάλμα κατά τη μετατροπή του αρχείου PDF %s:\n"

#: src/gtkgs.c:1744
#, c-format
msgid "Cannot open file %s.\n"
msgstr "Αδυναμία ανοίγματος αρχείου %s\n"

#: src/gtkgs.c:1746
msgid "File is not readable."
msgstr "Το αρχείο δεν μπορεί να αναγνωστεί."

#: src/gtkgs.c:1765
#, c-format
msgid "Error while scanning file %s\n"
msgstr "Σφάλμα κατά την ανάγνωση του αρχείου %s\n"

#: src/gtkgs.c:1768
msgid "The file is not a PostScript document."
msgstr "Το αρχείο δεν είναι ένα έγγραφο PostScript."

#: src/gtkgs.c:1800
msgid "Document loaded."
msgstr "Το έγγραφο φορτώθηκε."

#: src/main.c:75
#, c-format
msgid "Invalid geometry string \"%s\"\n"
msgstr "Μη έγκυρο αλφαριθμητικό γεωμετρίας \"%s\"\n"

#: src/main.c:189
msgid "Specify the number of empty windows to open."
msgstr "Καθορισμός του αριθμού των κενών παραθύρων για άνοιγμα."

#: src/main.c:190
msgid "Number of empty windows"
msgstr "Αριθμός των κενών παραθύρων"

#: src/main.c:192
msgid "X geometry specification (see \"X\" man page)."
msgstr "Καθορισμός γεωμετρίας X  (βλ. \"X\" man page)."

#: src/main.c:193
msgid "GEOMETRY"
msgstr "ΓΕΩΜΕΤΡΙΑ"

#: src/main.c:232
#, c-format
msgid "Failed to initialize GConf subsystem: %s"
msgstr "Αποτυχία αρχικοποίησης του υποσυστήματος GConf: %s"

#: src/main.c:238
msgid "Failed to initialize Bonobo!\n"
msgstr "Αποτυχία αρχικοποίησης του Bonobo!\n"

