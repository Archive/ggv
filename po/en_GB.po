# English (British) translation.
# Copyright (C) 2004 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ggv package.
# Gareth Owen <gowen72@yahoo.com>, 2004.
#
msgid ""
msgstr ""
"Project-Id-Version: ggv\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2005-01-30 21:07+0000\n"
"PO-Revision-Date: 2005-01-30 21:07+0100\n"
"Last-Translator: David Lodge <dave@cirt.net>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: bonobo/GNOME_GGV.server.in.in.h:1
msgid "GGV PostScript Document Control"
msgstr "GGV PostScript Document Control"

#: bonobo/GNOME_GGV.server.in.in.h:2
msgid "GGV PostScript View"
msgstr "GGV PostScript View"

#: bonobo/GNOME_GGV.server.in.in.h:3
msgid "GGV PostScript viewer factory"
msgstr "GGV PostScript viewer factory"

#: bonobo/ggv-control-ui.xml.h:2
#, no-c-format
msgid "15_0%"
msgstr "15_0%"

#: bonobo/ggv-control-ui.xml.h:3
msgid "1:1"
msgstr "1:1"

#: bonobo/ggv-control-ui.xml.h:5
#, no-c-format
msgid "25%"
msgstr "25%"

#: bonobo/ggv-control-ui.xml.h:6
msgid "Change zoom factor to default"
msgstr "Change zoom factor to default"

#: bonobo/ggv-control-ui.xml.h:7
msgid "Decrease zoom factor"
msgstr "Decrease zoom factor"

#: bonobo/ggv-control-ui.xml.h:8
msgid "Fit"
msgstr "Fit"

#: bonobo/ggv-control-ui.xml.h:9
msgid "Fit Width"
msgstr "Fit Width"

#: bonobo/ggv-control-ui.xml.h:10
msgid "Fit _Width"
msgstr "Fit _Width"

#: bonobo/ggv-control-ui.xml.h:11
msgid "Fit to Page _Size"
msgstr "Fit to Page _Size"

#: bonobo/ggv-control-ui.xml.h:12
msgid "Fit to Page _Width"
msgstr "Fit to Page _Width"

#: bonobo/ggv-control-ui.xml.h:13
msgid "In"
msgstr "In"

#: bonobo/ggv-control-ui.xml.h:14
msgid "Increase zoom factor"
msgstr "Increase zoom factor"

#: bonobo/ggv-control-ui.xml.h:15
msgid "Other"
msgstr "Other"

#: bonobo/ggv-control-ui.xml.h:16
msgid "Out"
msgstr "Out"

#: bonobo/ggv-control-ui.xml.h:17
msgid "Zoom _In"
msgstr "Zoom _In"

#: bonobo/ggv-control-ui.xml.h:18
msgid "Zoom _Out"
msgstr "Zoom _Out"

#: bonobo/ggv-control-ui.xml.h:19
msgid "Zoom the page to be as wide as the window"
msgstr "Zoom the page to be as wide as the window"

#: bonobo/ggv-control-ui.xml.h:20
msgid "Zoom the page to fit in the window"
msgstr "Zoom the page to fit in the window"

#: bonobo/ggv-control-ui.xml.h:22
#, no-c-format
msgid "_100%"
msgstr "_100%"

#: bonobo/ggv-control-ui.xml.h:24
#, no-c-format
msgid "_200%"
msgstr "_200%"

#: bonobo/ggv-control-ui.xml.h:26
#, no-c-format
msgid "_400%"
msgstr "_400%"

#: bonobo/ggv-control-ui.xml.h:28
#, no-c-format
msgid "_50%"
msgstr "_50%"

#: bonobo/ggv-control-ui.xml.h:30
#, no-c-format
msgid "_75%"
msgstr "_75%"

#: bonobo/ggv-control-ui.xml.h:31
msgid "_Auto-fit"
msgstr "_Auto-fit"

#: bonobo/ggv-control-ui.xml.h:32
msgid "_Default Zoom"
msgstr "_Default Zoom"

#: bonobo/ggv-control-ui.xml.h:33
msgid "_Fit Window"
msgstr "_Fit Window"

#: bonobo/ggv-control-ui.xml.h:34
msgid "_No Auto-fit"
msgstr "_No Auto-fit"

#: bonobo/ggv-control-ui.xml.h:35
msgid "_Zoom"
msgstr "_Zoom"

#: bonobo/ggv-postscript-view-ui.xml.h:1
#: src/ggvutils.c:59
msgid "10x14"
msgstr "10x14"

#: bonobo/ggv-postscript-view-ui.xml.h:2
#: src/ggvutils.c:49
msgid "A0"
msgstr "A0"

#: bonobo/ggv-postscript-view-ui.xml.h:3
#: src/ggvutils.c:50
msgid "A1"
msgstr "A1"

#: bonobo/ggv-postscript-view-ui.xml.h:4
#: src/ggvutils.c:51
msgid "A2"
msgstr "A2"

#: bonobo/ggv-postscript-view-ui.xml.h:5
#: src/ggvutils.c:52
msgid "A3"
msgstr "A3"

#: bonobo/ggv-postscript-view-ui.xml.h:6
#: src/ggvutils.c:53
msgid "A4"
msgstr "A4"

#: bonobo/ggv-postscript-view-ui.xml.h:7
#: src/ggvutils.c:54
msgid "A5"
msgstr "A5"

#: bonobo/ggv-postscript-view-ui.xml.h:8
#: src/ggvutils.c:55
msgid "B4"
msgstr "B4"

#: bonobo/ggv-postscript-view-ui.xml.h:9
#: src/ggvutils.c:56
msgid "B5"
msgstr "B5"

#: bonobo/ggv-postscript-view-ui.xml.h:10
#: src/ggvutils.c:42
msgid "BBox"
msgstr "BBox"

#: bonobo/ggv-postscript-view-ui.xml.h:11
#: src/ggvutils.c:48
msgid "Executive"
msgstr "Executive"

#: bonobo/ggv-postscript-view-ui.xml.h:12
msgid "First"
msgstr "First"

#: bonobo/ggv-postscript-view-ui.xml.h:13
#: src/ggvutils.c:57
msgid "Folio"
msgstr "Folio"

#: bonobo/ggv-postscript-view-ui.xml.h:14
msgid "Go to first page"
msgstr "Go to first page"

#: bonobo/ggv-postscript-view-ui.xml.h:15
msgid "Go to last page"
msgstr "Go to last page"

#: bonobo/ggv-postscript-view-ui.xml.h:16
msgid "Go to next page"
msgstr "Go to next page"

#: bonobo/ggv-postscript-view-ui.xml.h:17
msgid "Go to previous page"
msgstr "Go to previous page"

#: bonobo/ggv-postscript-view-ui.xml.h:18
msgid "Last"
msgstr "Last"

#: bonobo/ggv-postscript-view-ui.xml.h:19
#: src/ggvutils.c:45
msgid "Ledger"
msgstr "Ledger"

#: bonobo/ggv-postscript-view-ui.xml.h:20
#: src/ggvutils.c:46
msgid "Legal"
msgstr "Legal"

#: bonobo/ggv-postscript-view-ui.xml.h:21
#: src/ggvutils.c:43
msgid "Letter"
msgstr "Letter"

#: bonobo/ggv-postscript-view-ui.xml.h:22
msgid "Next"
msgstr "Next"

#: bonobo/ggv-postscript-view-ui.xml.h:23
msgid "Open the preferences dialog"
msgstr "Open the preferences dialogue"

#: bonobo/ggv-postscript-view-ui.xml.h:24
msgid "Post_Script Viewer Preferences"
msgstr "Post_Script Viewer Preferences"

#: bonobo/ggv-postscript-view-ui.xml.h:25
msgid "Prev"
msgstr "Prev"

#: bonobo/ggv-postscript-view-ui.xml.h:26
msgid "Print"
msgstr "Print"

#: bonobo/ggv-postscript-view-ui.xml.h:27
msgid "Print marked pages"
msgstr "Print marked pages"

#: bonobo/ggv-postscript-view-ui.xml.h:28
msgid "Print the whole document"
msgstr "Print the whole document"

#: bonobo/ggv-postscript-view-ui.xml.h:29
#: src/ggvutils.c:58
msgid "Quarto"
msgstr "Quarto"

#: bonobo/ggv-postscript-view-ui.xml.h:30
msgid "Save marked pages to a file"
msgstr "Save marked pages to a file"

#: bonobo/ggv-postscript-view-ui.xml.h:31
#: src/ggvutils.c:47
msgid "Statement"
msgstr "Statement"

#: bonobo/ggv-postscript-view-ui.xml.h:32
#: src/ggvutils.c:44
msgid "Tabloid"
msgstr "Tabloid"

#: bonobo/ggv-postscript-view-ui.xml.h:33
msgid "_Document Orientation"
msgstr "_Document Orientation"

#: bonobo/ggv-postscript-view-ui.xml.h:34
msgid "_Document Size"
msgstr "_Document Size"

#: bonobo/ggv-postscript-view-ui.xml.h:35
#: src/ggv-ui.xml.h:23
msgid "_Edit"
msgstr "_Edit"

#: bonobo/ggv-postscript-view-ui.xml.h:36
#: src/ggv-ui.xml.h:24
msgid "_File"
msgstr "_File"

#: bonobo/ggv-postscript-view-ui.xml.h:37
msgid "_First page"
msgstr "_First page"

#: bonobo/ggv-postscript-view-ui.xml.h:38
msgid "_Go"
msgstr "_Go"

#: bonobo/ggv-postscript-view-ui.xml.h:39
msgid "_Landscape"
msgstr "_Landscape"

#: bonobo/ggv-postscript-view-ui.xml.h:40
msgid "_Last page"
msgstr "_Last page"

#: bonobo/ggv-postscript-view-ui.xml.h:41
msgid "_Next page"
msgstr "_Next page"

#: bonobo/ggv-postscript-view-ui.xml.h:42
msgid "_Orientation"
msgstr "_Orientation"

#: bonobo/ggv-postscript-view-ui.xml.h:43
msgid "_Portrait"
msgstr "_Portrait"

#: bonobo/ggv-postscript-view-ui.xml.h:44
msgid "_Previous page"
msgstr "_Previous page"

#: bonobo/ggv-postscript-view-ui.xml.h:45
msgid "_Print document"
msgstr "_Print document"

#: bonobo/ggv-postscript-view-ui.xml.h:46
msgid "_Print marked pages"
msgstr "_Print marked pages"

#: bonobo/ggv-postscript-view-ui.xml.h:47
msgid "_Save marked pages"
msgstr "_Save marked pages"

#: bonobo/ggv-postscript-view-ui.xml.h:48
msgid "_Seascape"
msgstr "_Seascape"

#: bonobo/ggv-postscript-view-ui.xml.h:49
msgid "_Size"
msgstr "_Size"

#: bonobo/ggv-postscript-view-ui.xml.h:50
msgid "_Upside down"
msgstr "_Upside down"

#: bonobo/ggv-postscript-view-ui.xml.h:51
#: src/ggv-ui.xml.h:30
msgid "_View"
msgstr "_View"

#: bonobo/ggv-postscript-view.c:875
#, c-format
msgid ""
"Unable to execute print command:\n"
"%s"
msgstr ""
"Unable to execute print command:\n"
"%s"

#: bonobo/ggv-postscript-view.c:932
msgid ""
"No pages have been marked.\n"
"Do you want to save the whole document?"
msgstr ""
"No pages have been marked.\n"
"Do you want to save the whole document?"

#: bonobo/ggv-postscript-view.c:957
msgid "Select a file to save pages as"
msgstr "Select a file to save pages as"

#: bonobo/ggv-postscript-view.c:965
#: src/ggv-window.c:329
msgid "PostScript Documents"
msgstr "PostScript Documents"

#: bonobo/ggv-postscript-view.c:971
#: src/ggv-window.c:335
msgid "All Files"
msgstr "All Files"

#: bonobo/ggv-postscript-view.c:1010
msgid ""
"A file with this name already exists.\n"
"Do you want to overwrite it?"
msgstr ""
"A file with this name already exists.\n"
"Do you want to overwrite it?"

#: bonobo/ggv-postscript-view.c:1051
msgid ""
"No pages have been marked.\n"
"Do you want to print the whole document?"
msgstr ""
"No pages have been marked.\n"
"Do you want to print the whole document?"

#: bonobo/ggv-postscript-view.c:2118
msgid "GhostScript output"
msgstr "GhostScript output"

#: bonobo/ggv-postscript-view.c:2150
msgid "Document title"
msgstr "Document title"

#: bonobo/ggv-postscript-view.c:2153
msgid "GGV control status"
msgstr "GGV control status"

#: bonobo/ggv-postscript-view.c:2156
msgid "Number of pages"
msgstr "Number of pages"

#: bonobo/ggv-postscript-view.c:2159
msgid "Page names"
msgstr "Page names"

#: bonobo/ggv-postscript-view.c:2162
msgid "Current page number"
msgstr "Current page number"

#: bonobo/ggv-postscript-view.c:2165
msgid "Document width"
msgstr "Document width"

#: bonobo/ggv-postscript-view.c:2168
msgid "Document height"
msgstr "Document height"

#: bonobo/ggv-postscript-view.c:2172
msgid "Document orientation"
msgstr "Document orientation"

#: bonobo/ggv-postscript-view.c:2175
msgid "Default orientation"
msgstr "Default orientation"

#: bonobo/ggv-postscript-view.c:2179
msgid "Default size"
msgstr "Default size"

#: bonobo/ggv-postscript-view.c:2184
msgid "Override document orientation"
msgstr "Override document orientation"

#: bonobo/ggv-postscript-view.c:2188
msgid "Override document size"
msgstr "Override document size"

#: bonobo/ggv-postscript-view.c:2192
#: ggv.schemas.in.h:21
msgid "Respect EOF comment"
msgstr "Respect EOF comment"

#: bonobo/ggv-postscript-view.c:2196
msgid "Antialiasing"
msgstr "Antialiasing"

#: bonobo/ggv-sidebar.c:358
msgid "Toggle marked state of all pages"
msgstr "Toggle marked state of all pages"

#: bonobo/ggv-sidebar.c:359
msgid "Toggle marked state of all pages: previously marked pages will be unmarked and unmarked ones will become marked."
msgstr "Toggle marked state of all pages: previously marked pages will be unmarked and unmarked ones will become marked."

#: bonobo/ggv-sidebar.c:371
msgid "Toggle marked state of odd pages"
msgstr "Toggle marked state of odd pages"

#: bonobo/ggv-sidebar.c:372
msgid "Toggle marked state of odd pages: previously marked odd pages will be unmarked and unmarked ones will become marked."
msgstr "Toggle marked state of odd pages: previously marked odd pages will be unmarked and unmarked ones will become marked."

#: bonobo/ggv-sidebar.c:386
msgid "Toggle marked state of even pages"
msgstr "Toggle marked state of even pages"

#: bonobo/ggv-sidebar.c:387
msgid "Toggle marked state of even pages: previously marked even pages will be unmarked and unmarked ones will become marked."
msgstr "Toggle marked state of even pages: previously marked even pages will be unmarked and unmarked ones will become marked."

#: bonobo/ggv-sidebar.c:401
msgid "Clear marked state of all pages"
msgstr "Clear marked state of all pages"

#: bonobo/ggv-sidebar.c:402
msgid "Clear marked state of all pages: all pages will be unmarked."
msgstr "Clear marked state of all pages: all pages will be unmarked."

#: ggv.desktop.in.h:1
msgid "PostScript Viewer"
msgstr "PostScript Viewer"

#: ggv.desktop.in.h:2
msgid "View PostScript files"
msgstr "View PostScript files"

#: ggv.schemas.in.h:1
msgid "Antialiased rendering"
msgstr "Antialiased rendering"

#: ggv.schemas.in.h:2
msgid "Antialiasing arguments"
msgstr "Antialiasing arguments"

#: ggv.schemas.in.h:3
msgid "Autojump"
msgstr "Autojump"

#: ggv.schemas.in.h:4
msgid "Command line arguments for antialiasing."
msgstr "Command line arguments for antialiasing."

#: ggv.schemas.in.h:5
msgid "Command line used to execute the PostScript interpreter."
msgstr "Command line used to execute the PostScript interpreter."

#: ggv.schemas.in.h:6
msgid "Coordinate units"
msgstr "Coordinate units"

#: ggv.schemas.in.h:7
msgid "Default document orientation"
msgstr "Default document orientation"

#: ggv.schemas.in.h:8
msgid "Default page size"
msgstr "Default page size"

#: ggv.schemas.in.h:9
msgid "Default zoom factor"
msgstr "Default zoom factor"

#: ggv.schemas.in.h:10
msgid "If true, the orientation specified by the document will be overriden with the default orientation."
msgstr "If true, the orientation specified by the document will be overriden with the default orientation."

#: ggv.schemas.in.h:11
msgid "Override document specified orientation"
msgstr "Override document specified orientation"

#: ggv.schemas.in.h:12
msgid "Override document specified page size"
msgstr "Override document specified page size"

#: ggv.schemas.in.h:13
msgid "PDF to DSC conversion command"
msgstr "PDF to DSC conversion command"

#: ggv.schemas.in.h:14
msgid "PDF to PS conversion command"
msgstr "PDF to PS conversion command"

#: ggv.schemas.in.h:15
msgid "Page flip"
msgstr "Page flip"

#: ggv.schemas.in.h:16
msgid "Panel on right-hand side"
msgstr "Panel on right-hand side"

#: ggv.schemas.in.h:17
msgid "Percentage of the viewable area to move when scrolling."
msgstr "Percentage of the viewable area to move when scrolling."

#: ggv.schemas.in.h:18
msgid "Percentage of the viewable area to scroll"
msgstr "Percentage of the viewable area to scroll"

#: ggv.schemas.in.h:19
msgid "PostScript interpreter command"
msgstr "PostScript interpreter command"

#: ggv.schemas.in.h:20
msgid "Printing command"
msgstr "Printing command"

#: ggv.schemas.in.h:22
msgid "Save window geometry"
msgstr "Save window geometry"

#: ggv.schemas.in.h:23
msgid "Show menubar"
msgstr "Show menubar"

#: ggv.schemas.in.h:24
msgid "Show previous visible area when scrolling"
msgstr "Show previous visible area when scrolling"

#: ggv.schemas.in.h:25
msgid "Show side panel"
msgstr "Show side panel"

#: ggv.schemas.in.h:26
msgid "Show statusbar"
msgstr "Show statusbar"

#: ggv.schemas.in.h:27
msgid "Show toolbar"
msgstr "Show toolbar"

#: ggv.schemas.in.h:28
msgid "The command line used to convert PDF files to PostScript."
msgstr "The command line used to convert PDF files to PostScript."

#: ggv.schemas.in.h:29
msgid "The command line used to create PostScript DSC files from PDF files."
msgstr "The command line used to create PostScript DSC files from PDF files."

#: ggv.schemas.in.h:30
msgid "The command used to unpack files compressed with bzip2."
msgstr "The command used to unpack files compressed with bzip2."

#: ggv.schemas.in.h:31
msgid "The command used to unpack files compressed with gzip."
msgstr "The command used to unpack files compressed with gzip."

#: ggv.schemas.in.h:32
msgid "The default page size to use when no page size is specified by the document or when the user wishes to override the size specified in the document."
msgstr "The default page size to use when no page size is specified by the document or when the user wishes to override the size specified in the document."

#: ggv.schemas.in.h:33
msgid "The default zoom factor for the document. The value should belong to the interval [0.05, 30.0]."
msgstr "The default zoom factor for the document. The value should belong to the interval [0.05, 30.0]."

#: ggv.schemas.in.h:34
msgid "The height of the viewer's window."
msgstr "The height of the viewer's window."

#: ggv.schemas.in.h:35
msgid "The orientation to use when the document defines no orientation or when the user wishes to override the document specified orientation."
msgstr "The orientation to use when the document defines no orientation or when the user wishes to override the document specified orientation."

#: ggv.schemas.in.h:36
msgid "The shell command used to print files."
msgstr "The shell command used to print files."

#: ggv.schemas.in.h:37
msgid "The width of the viewer's window."
msgstr "The width of the viewer's window."

#: ggv.schemas.in.h:38
msgid "Toolbar style"
msgstr "Toolbar style"

#: ggv.schemas.in.h:39
msgid "Unbzip2 command"
msgstr "Unbzip2 command"

#: ggv.schemas.in.h:40
msgid "Ungzip command"
msgstr "Ungzip command"

#: ggv.schemas.in.h:41
msgid "Units for the coordinate display."
msgstr "Units for the coordinate display."

#: ggv.schemas.in.h:42
msgid "Viewer window height"
msgstr "Viewer window height"

#: ggv.schemas.in.h:43
msgid "Viewer window width"
msgstr "Viewer window width"

#: ggv.schemas.in.h:44
msgid "Watch document for changes"
msgstr "Watch document for changes"

#: ggv.schemas.in.h:45
msgid "Whether GGV should automatically flip pages when you scroll to the edge."
msgstr "Whether GGV should automatically flip pages when you scroll to the edge."

#: ggv.schemas.in.h:46
msgid "Whether the PostScript display widget should poll the viewed file for changes and reload it if it changes."
msgstr "Whether the PostScript display widget should poll the viewed file for changes and reload it if it changes."

#: ggv.schemas.in.h:47
msgid "Whether the PostScript display widget should use its default page size instead of the one specified in the document."
msgstr "Whether the PostScript display widget should use its default page size instead of the one specified in the document."

#: ggv.schemas.in.h:48
msgid "Whether the PostScript display widget will respect the End Of File comment. If true, the widget will only parse the document until EOF comment."
msgstr "Whether the PostScript display widget will respect the End Of File comment. If true, the widget will only parse the document until EOF comment."

#: ggv.schemas.in.h:49
msgid "Whether the control should jump to next page when using keyboard to scroll."
msgstr "Whether the control should jump to next page when using keyboard to scroll."

#: ggv.schemas.in.h:50
msgid "Whether the document should be rendered in an antialiased mode."
msgstr "Whether the document should be rendered in an antialiased mode."

#: ggv.schemas.in.h:51
msgid "Whether the menubar should be visible by default."
msgstr "Whether the menubar should be visible by default."

#: ggv.schemas.in.h:52
msgid "Whether the side panel should appear on the right of the display area."
msgstr "Whether the side panel should appear on the right of the display area."

#: ggv.schemas.in.h:53
msgid "Whether the side panel should be visible by default."
msgstr "Whether the side panel should be visible by default."

#: ggv.schemas.in.h:54
msgid "Whether the statusbar should be visible by default."
msgstr "Whether the statusbar should be visible by default."

#: ggv.schemas.in.h:55
msgid "Whether the toolbar buttons show icons, text, both or use the GNOME-wide default."
msgstr "Whether the toolbar buttons show icons, text, both or use the GNOME-wide default."

#: ggv.schemas.in.h:56
msgid "Whether the toolbar should be visible by default."
msgstr "Whether the toolbar should be visible by default."

#: ggv.schemas.in.h:57
msgid "Whether the user wants to see a rectangle outlining the previously visible area when scrolling."
msgstr "Whether the user wants to see a rectangle outlining the previously visible area when scrolling."

#: ggv.schemas.in.h:58
msgid "Whether the viewer should save and restore the window geometry."
msgstr "Whether the viewer should save and restore the window geometry."

#: src/ggv-prefs-ui.c:523
msgid "_Use GNOME defaults"
msgstr "_Use GNOME defaults"

#: src/ggv-prefs-ui.c:524
msgid "Show _both icons and text"
msgstr "Show _both icons and text"

#: src/ggv-prefs-ui.c:525
msgid "Show only _icons"
msgstr "Show only _icons"

#: src/ggv-prefs-ui.c:526
msgid "Show only t_ext"
msgstr "Show only t_ext"

#: src/ggv-prefs-ui.c:559
msgid "GGV Preferences"
msgstr "GGV Preferences"

#. zoom choice menu
#: src/ggv-prefs-ui.c:565
msgid "Default _Zoom:"
msgstr "Default _Zoom:"

#. auto-fit choice menu
#: src/ggv-prefs-ui.c:593
msgid "A_uto-fit mode:"
msgstr "A_uto-fit mode:"

#. units choice menu
#: src/ggv-prefs-ui.c:618
msgid "Coordinate _units:"
msgstr "Coordinate _units:"

#. size choice menu
#: src/ggv-prefs-ui.c:642
msgid "_Fallback page size:"
msgstr "_Fallback page size:"

#: src/ggv-prefs-ui.c:665
msgid "Override _document size"
msgstr "Override _document size"

#. orientation choice menu
#: src/ggv-prefs-ui.c:674
msgid "Fallback _media orientation:"
msgstr "Fallback _media orientation:"

#: src/ggv-prefs-ui.c:698
msgid "O_verride document orientation"
msgstr "O_verride document orientation"

#. antialiasing
#: src/ggv-prefs-ui.c:707
msgid "A_ntialiasing"
msgstr "A_ntialiasing"

#: src/ggv-prefs-ui.c:718
msgid "_Respect EOF comments"
msgstr "_Respect EOF comments"

#. watch file
#: src/ggv-prefs-ui.c:727
msgid "_Watch file"
msgstr "_Watch file"

#: src/ggv-prefs-ui.c:736
msgid "Document"
msgstr "Document"

#. show side panel
#: src/ggv-prefs-ui.c:745
msgid "_Show side panel"
msgstr "_Show side panel"

#: src/ggv-prefs-ui.c:756
msgid "_Put side panel on the right-hand side"
msgstr "_Put side panel on the right-hand side"

#. show menubar
#: src/ggv-prefs-ui.c:765
msgid "Show _menubar"
msgstr "Show _menubar"

#. show toolbar
#: src/ggv-prefs-ui.c:774
msgid "Show _toolbar"
msgstr "Show _toolbar"

#. show statusbar
#: src/ggv-prefs-ui.c:784
msgid "Show statusba_r"
msgstr "Show statusba_r"

#. save geometry
#: src/ggv-prefs-ui.c:794
msgid "Save _geometry"
msgstr "Save _geometry"

#: src/ggv-prefs-ui.c:803
msgid "Layout"
msgstr "Layout"

#: src/ggv-prefs-ui.c:814
msgid "_Jump to beginning of page"
msgstr "_Jump to beginning of page"

#: src/ggv-prefs-ui.c:823
msgid "Automatically _flip pages"
msgstr "Automatically _flip pages"

#: src/ggv-prefs-ui.c:834
msgid "Outline _last visible part when scrolling"
msgstr "Outline _last visible part when scrolling"

#: src/ggv-prefs-ui.c:845
msgid "Amount of _visible area to scroll"
msgstr "Amount of _visible area to scroll"

#: src/ggv-prefs-ui.c:858
msgid "Navigation"
msgstr "Navigation"

#. interpreter
#: src/ggv-prefs-ui.c:867
msgid "_Interpreter:"
msgstr "_Interpreter:"

#. antialiasing
#: src/ggv-prefs-ui.c:887
msgid "A_ntialiasing:"
msgstr "A_ntialiasing:"

#. PDF->DSC command
#: src/ggv-prefs-ui.c:907
msgid "Convert _PDF to DSC file:"
msgstr "Convert _PDF to DSC file:"

#. scan PDF command
#: src/ggv-prefs-ui.c:931
msgid "Convert _PDF to PS:"
msgstr "Convert _PDF to PS:"

#. unzip command: gzip
#: src/ggv-prefs-ui.c:955
msgid "_Gzip:"
msgstr "_Gzip:"

#. unzip command: bzip2
#: src/ggv-prefs-ui.c:975
msgid "_Bzip2:"
msgstr "_Bzip2:"

#: src/ggv-prefs-ui.c:994
msgid "Ghostscript"
msgstr "Ghostscript"

#. print command
#: src/ggv-prefs-ui.c:1003
msgid "_Print command:"
msgstr "_Print command:"

#: src/ggv-prefs-ui.c:1021
msgid "Printing"
msgstr "Printing"

#: src/ggv-ui.xml.h:1
msgid "About this application"
msgstr "About this application"

#: src/ggv-ui.xml.h:2
msgid "Cancel"
msgstr "Cancel"

#: src/ggv-ui.xml.h:3
msgid "Close this window"
msgstr "Close this window"

#: src/ggv-ui.xml.h:4
msgid "Help for this application"
msgstr "Help for this application"

#: src/ggv-ui.xml.h:5
msgid "Open"
msgstr "Open"

#: src/ggv-ui.xml.h:6
msgid "Open a new PostScript document"
msgstr "Open a new PostScript document"

#: src/ggv-ui.xml.h:7
msgid "Open in new window"
msgstr "Open in new window"

#: src/ggv-ui.xml.h:8
msgid "Open in this window"
msgstr "Open in this window"

#: src/ggv-ui.xml.h:9
msgid "Reload"
msgstr "Reload"

#: src/ggv-ui.xml.h:10
msgid "Reload current document"
msgstr "Reload current document"

#: src/ggv-ui.xml.h:11
msgid "Show Side_bar"
msgstr "Show Side_bar"

#: src/ggv-ui.xml.h:12
msgid "Show _Menus"
msgstr "Show _Menus"

#: src/ggv-ui.xml.h:13
msgid "Show _Statusbar"
msgstr "Show _Statusbar"

#: src/ggv-ui.xml.h:14
msgid "Show _Toolbar"
msgstr "Show _Toolbar"

#: src/ggv-ui.xml.h:15
msgid "Show/hide the menus"
msgstr "Show/hide the menus"

#: src/ggv-ui.xml.h:16
msgid "Show/hide the sidebar"
msgstr "Show/hide the sidebar"

#: src/ggv-ui.xml.h:17
msgid "Show/hide the statusbar"
msgstr "Show/hide the statusbar"

#: src/ggv-ui.xml.h:18
msgid "Show/hide the toolbar"
msgstr "Show/hide the toolbar"

#: src/ggv-ui.xml.h:19
msgid "Toggle fullscreen mode"
msgstr "Toggle fullscreen mode"

#: src/ggv-ui.xml.h:20
msgid "_About"
msgstr "_About"

#: src/ggv-ui.xml.h:21
msgid "_Close"
msgstr "_Close"

#: src/ggv-ui.xml.h:22
msgid "_Contents"
msgstr "_Contents"

#: src/ggv-ui.xml.h:25
msgid "_Full Screen"
msgstr "_Full Screen"

#: src/ggv-ui.xml.h:26
msgid "_Help"
msgstr "_Help"

#: src/ggv-ui.xml.h:27
msgid "_Layout"
msgstr "_Layout"

#: src/ggv-ui.xml.h:28
msgid "_Open"
msgstr "_Open"

#: src/ggv-ui.xml.h:29
msgid "_Reload"
msgstr "_Reload"

#: src/ggv-window.c:266
#: src/ggv-window.c:375
#: src/ggv-window.c:606
#: src/ggv-window.c:646
#: src/ggv-window.c:954
#, c-format
msgid ""
"Unable to load file:\n"
"%s"
msgstr ""
"Unable to load file:\n"
"%s"

#: src/ggv-window.c:302
#: src/ggv-window.c:1036
msgid "GGV: no document loaded"
msgstr "GGV: no document loaded"

#: src/ggv-window.c:318
msgid "Select a PostScript document"
msgstr "Select a PostScript document"

#: src/ggv-window.c:410
#, c-format
msgid ""
"Unable to reload file:\n"
"%s"
msgstr ""
"Unable to reload file:\n"
"%s"

#: src/ggv-window.c:521
msgid "Jaka Mocnik (current maintainer)"
msgstr "Jaka Mocnik (current maintainer)"

#: src/ggv-window.c:531
msgid "And many more..."
msgstr "And many more..."

#: src/ggv-window.c:538
msgid "translator-credits"
msgstr ""
"Abigail Brady <morwen@evilmagic.org>\n"
"Bastien Nocera <hadess@hadess.net>\n"
"Gareth Owen <gowen72@yahoo.com>\n"
"David Lodge <dave@cirt.net>"

#: src/ggv-window.c:552
msgid "Gnome Ghostview"
msgstr "Gnome Ghostview"

#: src/ggv-window.c:555
msgid "The GNOME PostScript document previewer"
msgstr "The GNOME PostScript document previewer"

#: src/ggv-window.c:979
msgid "Open recent files"
msgstr "Open recent files"

#: src/ggv-window.c:1079
msgid "Open a file."
msgstr "Open a file."

#: src/ggv-window.c:1231
msgid " - GGV"
msgstr " - GGV"

#: src/ggv-window.c:1322
#, c-format
msgid ""
"Unable to reload file after it changed:\n"
"%s"
msgstr ""
"Unable to reload file after it changed:\n"
"%s"

#: src/ggvutils.c:71
msgid "Portrait"
msgstr "Portrait"

#: src/ggvutils.c:72
msgid "Landscape"
msgstr "Landscape"

#: src/ggvutils.c:73
msgid "Upside Down"
msgstr "Upside Down"

#: src/ggvutils.c:74
msgid "Seascape"
msgstr "Seascape"

#: src/ggvutils.c:82
msgid "inch"
msgstr "inch"

#: src/ggvutils.c:83
msgid "mm"
msgstr "mm"

#: src/ggvutils.c:84
msgid "cm"
msgstr "cm"

#: src/ggvutils.c:85
msgid "point"
msgstr "point"

#: src/ggvutils.c:107
msgid "None"
msgstr "None"

#: src/ggvutils.c:107
msgid "Fit to page width"
msgstr "Fit to page width"

#: src/ggvutils.c:107
msgid "Fit to page size"
msgstr "Fit to page size"

#: src/ggvutils.c:317
#, c-format
msgid "Unable to load ggv stock icon '%s'\n"
msgstr "Unable to load ggv stock icon '%s'\n"

#: src/gtkgs.c:379
msgid "No document loaded."
msgstr "No document loaded."

#: src/gtkgs.c:492
msgid "File is not a valid PostScript document."
msgstr "File is not a valid PostScript document."

#: src/gtkgs.c:1019
msgid "Broken pipe."
msgstr "Broken pipe."

#: src/gtkgs.c:1208
msgid "Interpreter failed."
msgstr "Interpreter failed."

#. report error
#: src/gtkgs.c:1312
#, c-format
msgid "Error while decompressing file %s:\n"
msgstr "Error while decompressing file %s:\n"

#: src/gtkgs.c:1418
#, c-format
msgid "Error while converting pdf file %s:\n"
msgstr "Error while converting pdf file %s:\n"

#: src/gtkgs.c:1744
#, c-format
msgid "Cannot open file %s.\n"
msgstr "Cannot open file %s.\n"

#: src/gtkgs.c:1746
msgid "File is not readable."
msgstr "File is not readable."

#: src/gtkgs.c:1765
#, c-format
msgid "Error while scanning file %s\n"
msgstr "Error while scanning file %s\n"

#: src/gtkgs.c:1768
msgid "The file is not a PostScript document."
msgstr "The file is not a PostScript document."

#: src/gtkgs.c:1800
msgid "Document loaded."
msgstr "Document loaded."

#: src/main.c:75
#, c-format
msgid "Invalid geometry string \"%s\"\n"
msgstr "Invalid geometry string \"%s\"\n"

#: src/main.c:199
msgid "Specify the number of empty windows to open."
msgstr "Specify the number of empty windows to open."

#: src/main.c:200
msgid "Number of empty windows"
msgstr "Number of empty windows"

#: src/main.c:202
msgid "X geometry specification (see \"X\" man page)."
msgstr "X geometry specification (see \"X\" man page)."

#: src/main.c:203
msgid "GEOMETRY"
msgstr "GEOMETRY"

#: src/main.c:242
#, c-format
msgid "Failed to initialize GConf subsystem: %s"
msgstr "Failed to initialise GConf subsystem: %s"

#: src/main.c:248
msgid "Failed to initialize Bonobo!\n"
msgstr "Failed to initialise Bonobo!\n"

