/*
 * ggv-control.h
 *
 * Author:  Jaka Mocnik  <jaka@gnu.org>
 *
 * Copyright (c) 2001 - 2005 Free Software Foundation
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
 * License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef _GGV_CONTROL_H_
#define _GGV_CONTROL_H_

#include <ggv-postscript-view.h>

G_BEGIN_DECLS

#define GGV_CONTROL_TYPE           (ggv_control_get_type ())
#define GGV_CONTROL(o)             (GTK_CHECK_CAST ((o), GGV_CONTROL_TYPE, GgvControl))
#define GGV_CONTROL_CLASS(k)       (GTK_CHECK_CLASS_CAST((k), GGV_CONTROL_TYPE, GgvControlClass))
#define GGV_IS_CONTROL(o)          (GTK_CHECK_TYPE ((o), GGV_CONTROL_TYPE))
#define GGV_IS_CONTROL_CLASS(k)    (GTK_CHECK_CLASS_TYPE ((k), GGV_CONTROL_TYPE))

typedef struct _GgvControl GgvControl;
typedef struct _GgvControlClass GgvControlClass;
typedef struct _GgvControlPrivate GgvControlPrivate;
typedef struct _GgvControlClassPrivate GgvControlClassPrivate;

struct _GgvControl {
  BonoboControl control;

  GgvControlPrivate *priv;
};

struct _GgvControlClass {
  BonoboControlClass parent_class;

  GgvControlClassPrivate *priv;
};

GtkType ggv_control_get_type(void);
GgvControl *ggv_control_new(GgvPostScriptView * ps_view);
GgvControl *ggv_control_construct(GgvControl * control,
                                  GgvPostScriptView * ps_view);

G_END_DECLS

#endif /* _GGV_CONTROL */
